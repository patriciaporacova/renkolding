# RenKolding management system

This piece of software is a bachelor thesis of 3 VIA University College engineering students which serves as a management system for cleaning company in Kolding, Denmark. The frontend is written in Vue.js, backend in .NET Core and database in MySql.

## Installation

This project is fully dockerized and can be started easily with (need to have Docker and docker compose installed)

```bash
docker-compose up -d
```

If you with to start up only front end side for dev purposes run command (need to have vue CLI and node installed)
```bash
npm i 
```
```bash
npm run serve
```
Then go to 
```bash
http://localhost:8080/
```

If you with to start up only back end side for dev purposes run command (need to have .net core installed and dotnet tools)
```bash
dotnet run 
```
For a swagger API docs, visit
```bash
https://localhost:5001/index.html
```

## Screenshots 
### Dashboard
![Screenshot_2021-06-04_at_10.29.45](/uploads/8cc80f2cc2e744ff23f049f381a906be/Screenshot_2021-06-04_at_10.29.45.png)

### Dashboard Edit Form
![Screenshot_2021-06-04_at_10.30.13](/uploads/434521aef203ca42a363d61d528c9061/Screenshot_2021-06-04_at_10.30.13.png)

### Calendar
![Screenshot_2021-06-04_at_10.30.24](/uploads/178816658feb55eb1f3975c3d9978c2f/Screenshot_2021-06-04_at_10.30.24.png)

### Calendar item detail
![Screenshot_2021-06-04_at_10.30.36](/uploads/8c6dd88eff5b697bec56cf28a314a039/Screenshot_2021-06-04_at_10.30.36.png)

### Calendar add
![Screenshot_2021-06-04_at_10.30.54](/uploads/f88c85158d25f1a4fef8b19c47e4e1a2/Screenshot_2021-06-04_at_10.30.54.png)

### Calendar edit
![Screenshot_2021-06-04_at_10.31.07](/uploads/084d6c62b998a465a837ee5930683da1/Screenshot_2021-06-04_at_10.31.07.png)

### Calendar day view
![Screenshot_2021-06-04_at_10.31.24](/uploads/ddf7d090d1aaad40e034f117aed177f7/Screenshot_2021-06-04_at_10.31.24.png)

### Calendar employee view
![Screenshot_2021-06-04_at_10.32.30](/uploads/68451f20f48ad40d2841c8429f74a5c2/Screenshot_2021-06-04_at_10.32.30.png)

### Calendar employee mobile view
![Screenshot_2021-06-04_at_10.32.51](/uploads/4fdba3dded1a8ac9aa8acb323e5e9879/Screenshot_2021-06-04_at_10.32.51.png)

### Login
![Screenshot_2021-06-04_at_10.33.08](/uploads/b3c7b0999d314917d2eadfd112fbdc37/Screenshot_2021-06-04_at_10.33.08.png)

## Development
Always do the following steps in your before pushing something

- commit the changes in your local branch (naming conventions in next part)
- checkout master branch
- pull changes
- checkout and rebase onto your branch
- resolve conflicts
- commit again (please check all your changes again)
- force push (otherwise you would have to do the mergin process all over again)
- create merge request 

thats it thank you :) 

## Naming conventions
call your branches as follow 
- if the branch container new feature:

```bash
 <issue number>/feature/<some descriptive feature name>
```
- if it is a fix for bug: 
```bash
 <issue number>/bug/<some descriptive bug name>
```

etc.
