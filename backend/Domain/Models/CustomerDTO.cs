﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Domain.Models
{
   public  class CustomerDTO
    {
        public int Id { get; set; }              
        public string Name { get; set; }        
        public string Cvr { get; set; }
        public string UIColorCode { get; set; }
        public string Phone { get; set; }      
        public string Email { get; set; }  
        public string Contactperson { get; set; }      
        public bool IsBusiness { get; set; }      
        public bool IsActive { get; set; }     
        public AddressDTO Address { get; set; }
        public List<ProjectDTO> Projects { get; set; }

        public CustomerDTO() { }
    }
}
