﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Models
{
  public  class DurationDTO
    {

        public DateTime WorkingDate { get; set; }
        public int ProjectId { get; set; }
        public string Duration { get; set; }
    }
}
