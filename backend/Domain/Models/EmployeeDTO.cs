﻿
using System.ComponentModel.DataAnnotations;

namespace Domain.Models
{
    public class EmployeeDTO
    {
        public int Id { get; set; }   
        public string FirstName { get; set; }    
        public string LastName { get; set; }       
        public string Phone { get; set; }
        public byte[] Picture { get; set; }        
        public bool IsActive { get; set; }    
        public string UIColorCode { get; set; }      
        public AddressDTO Address { get; set; }  
        //public virtual UserDTO User { get; set; }

      


    }
}
