﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Models
{
    public class MaterialDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public decimal? Price { get; set; }
    }
}
