﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Domain.Models
{
    public class ProjectDTO
    {
        public int Id { get; set; }
        public int CustomerId { get; set; }        
        public string Name { get; set; }
        public string UIColorCode { get; set; }
        public bool? IsHourlyPaid { get; set; }
        public decimal? HourlyRate { get; set; }
        public decimal? PricePerPiece { get; set; }
        public string ProjectDescription { get; set; }
        public string TaskDescription { get; set; }      
        public bool IsActive { get; set; }
        public AddressDTO Address { get; set; }
      
       
      
    }
}
