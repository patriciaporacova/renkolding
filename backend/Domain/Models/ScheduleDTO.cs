﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Domain.Models
{
    public class ScheduleDTO
    {
        public int Id { get; set; }
        public DateTime WorkingDate { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public string TodayTasks { get; set; }
        public int CustomerId { get; set; }
        public int ProjectId { get; set; }
        public int[] EmployeeIds { get; set; }
        [DefaultValue(false)]
        public bool IsDone { get; set; }

        public ScheduleDTO() { }

    }
}
