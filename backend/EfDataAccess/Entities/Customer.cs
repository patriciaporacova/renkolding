﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace EfDataAccess.Entities
{
   public  class Customer
    {
        public int Id { get; set; }

    
        [MaxLength(200)]
        public string Name { get; set; }
      
        [MaxLength(10)]
        public string Cvr { get; set; }
        [Required]
        [MaxLength(15)]
        public string Phone { get; set; }
        [Required]
        [MaxLength(200)]
        public string Email { get; set; }
    
        [MaxLength(200)]
        public string Contactperson { get; set; }
        [Required]
        public bool IsBusiness { get; set; }
        [Required]
        [DefaultValue(1)]
        public bool IsActive { get; set; }

        public string UIColorCode { get; set; }

        public int? AddressId { get; set; }
        public Address Address { get; set; }
        public List<Project> Projects { get; set; } 
    }
}
