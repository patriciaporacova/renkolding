﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Drawing;
using System.Text;

namespace EfDataAccess.Entities
{
    public class Employee
    {
        public int Id { get; set; }
        [Required]
        [MaxLength(200)]
        public string FirstName { get; set; }
        [Required]
        [MaxLength(200)]
        public string LastName { get; set; }
        [Required]
        [MaxLength(15)]
        public string Phone { get; set; }
        public byte[] Picture { get; set; }
        [Required]
        [DefaultValue(1)]
        public bool IsActive { get; set; }
        [MaxLength(50)]
        public string UIColorCode { get; set; }

        public int AddressId { get; set; }
        public Address Address { get; set; }

    
        public virtual int UserId { get; set; }
        public virtual User User { get; set; }


        public List<Schedule> ScheduledJobs { get; set; }


    }
}
