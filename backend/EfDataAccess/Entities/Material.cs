﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EfDataAccess.Entities
{
   public class Material
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public decimal? Price { get; set; }

        public IList<Schedule> Schedules { get; set; }


    }
}
