﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace EfDataAccess.Entities
{
    public class Project
    {
        public int Id { get; set; }
        [Required]
        [MaxLength(200)]
        public string Name { get; set; }
        [Required]
        public bool? IsHourlyPaid { get; set; }
        [Column(TypeName = "decimal(6,2)")]
        public decimal? HourlyRate { get; set; }
        [Column(TypeName = "decimal(6,2)")] 
        public decimal? PricePerPiece { get; set; }
        public string ProjectDescription { get; set; }
        public string TaskDescription { get; set; }
        [Required]
        [DefaultValue(1)]
        public bool IsActive { get; set; }

        public string UIColorCode { get; set; }

        public int AddressId { get; set; }
        public Address Address { get; set; }


        public int CustomerId { get; set; }

        


    }
}
