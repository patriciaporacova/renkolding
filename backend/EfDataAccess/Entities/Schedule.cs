﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace EfDataAccess.Entities
{
    public class Schedule
    {
        public int Id { get; set; }
        [Column(TypeName = "Date")]
        public DateTime WorkingDate { get; set; }
        public TimeSpan? StartTime { get; set; }
        public TimeSpan? EndTime { get; set; }
        public string TodayTasks { get; set; }
        public bool IsDone { get; set; }
        public IList<Employee> Employees { get; set; }
        public IList<Material> Materials { get; set; }
        public int CustomerId { get; set; }
        public virtual Customer Customer { get; set; }
        public int ProjectId { get; set; }
        public virtual Project Project { get; set; }

    }
}
