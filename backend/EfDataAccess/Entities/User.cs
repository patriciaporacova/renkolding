﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace EfDataAccess.Entities
{
    public class User
    {
        public int Id { get; set; }
        [Required]
        [MaxLength(200)]
        public string Username { get; set; }
        [Required]
        [MaxLength(200)]
        public string Password { get; set; }

        [Required]
        public bool IsAdmin { get; set; }


      

    }
}
