﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace EfDataAccess.Migrations
{
    public partial class FirstMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Addresses",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    AddressLine1 = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: false),
                    AddressLine2 = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: true),
                    City = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    Zip = table.Column<string>(type: "varchar(10)", maxLength: 10, nullable: false),
                    Country = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Addresses", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Username = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: false),
                    Password = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: false),
                    IsAdmin = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Customers",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: false),
                    Cvr = table.Column<string>(type: "nvarchar(10)", maxLength: 10, nullable: true),
                    Phone = table.Column<string>(type: "nvarchar(15)", maxLength: 15, nullable: false),
                    Email = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: false),
                    Contactperson = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: true),
                    IsBusiness = table.Column<bool>(type: "bit", nullable: false),
                    IsActive = table.Column<bool>(type: "bit", nullable: false),
                    AddressId = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Customers", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Customers_Addresses_AddressId",
                        column: x => x.AddressId,
                        principalTable: "Addresses",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Employees",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FirstName = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: false),
                    LastName = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: false),
                    Phone = table.Column<string>(type: "nvarchar(15)", maxLength: 15, nullable: false),
                    Picture = table.Column<byte[]>(type: "varbinary(max)", nullable: true),
                    IsActive = table.Column<bool>(type: "bit", nullable: false),
                    UIColorCode = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    AddressId = table.Column<int>(type: "int", nullable: false),
                    UserId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Employees", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Employees_Addresses_AddressId",
                        column: x => x.AddressId,
                        principalTable: "Addresses",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Employees_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Projects",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: false),
                    IsHourlyPaid = table.Column<bool>(type: "bit", nullable: false),
                    HourlyRate = table.Column<decimal>(type: "decimal(6,2)", nullable: true),
                    PricePerPiece = table.Column<decimal>(type: "decimal(6,2)", nullable: true),
                    ProjectDescription = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    TaskDescription = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IsActive = table.Column<bool>(type: "bit", nullable: false),
                    AddressId = table.Column<int>(type: "int", nullable: false),
                    CustomerId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Projects", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Projects_Addresses_AddressId",
                        column: x => x.AddressId,
                        principalTable: "Addresses",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Projects_Customers_CustomerId",
                        column: x => x.CustomerId,
                        principalTable: "Customers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Schedules",
                columns: table => new
                {
                    ID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    WorkingDate = table.Column<DateTime>(type: "Date", nullable: false),
                    StartTime = table.Column<TimeSpan>(type: "time", nullable: true),
                    EndTime = table.Column<TimeSpan>(type: "time", nullable: true),
                    TodayTasks = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CustomerId = table.Column<int>(type: "int", nullable: true),
                    ProjectId = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Schedules", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Schedules_Customers_CustomerId",
                        column: x => x.CustomerId,
                        principalTable: "Customers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Schedules_Projects_ProjectId",
                        column: x => x.ProjectId,
                        principalTable: "Projects",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "EmployeeSchedule",
                columns: table => new
                {
                    EmployeesId = table.Column<int>(type: "int", nullable: false),
                    ScheduledJobsID = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EmployeeSchedule", x => new { x.EmployeesId, x.ScheduledJobsID });
                    table.ForeignKey(
                        name: "FK_EmployeeSchedule_Employees_EmployeesId",
                        column: x => x.EmployeesId,
                        principalTable: "Employees",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_EmployeeSchedule_Schedules_ScheduledJobsID",
                        column: x => x.ScheduledJobsID,
                        principalTable: "Schedules",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Addresses",
                columns: new[] { "Id", "AddressLine1", "AddressLine2", "City", "Country", "Zip" },
                values: new object[,]
                {
                    { 1, "Langelinie 34", null, "Kolding", "Danmark", "6000" },
                    { 13, "Tøndervej 75", null, "Kolding", "Danmark", "6000" },
                    { 12, "Odensevej 5", null, "Kolding", "Danmark", "6000" },
                    { 11, "Pillevej 4", null, "Kolding", "Danmark", "6000" },
                    { 9, "Platinvej 8", null, "Kolding", "Danmark", "6000" },
                    { 8, "Marsvej 6", null, "Kolding", "Danmark", "6000" },
                    { 10, "Fabrikvej 3", null, "Kolding", "Danmark", "6000" },
                    { 6, "Tunøvænget 31", null, "Kolding", "Danmark", "6000" },
                    { 5, "Haderslevvej 134", null, "Kolding", "Danmark", "6000" },
                    { 4, "Fynsvej 8", null, "Kolding", "Danmark", "6000" },
                    { 3, "Vejlevej 67", null, "Kolding", "Danmark", "6000" },
                    { 2, "Sommervej 34", null, "Kolding", "Danmark", "6000" },
                    { 7, "Strandvej 3", null, "Kolding", "Danmark", "6000" }
                });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "IsAdmin", "Password", "Username" },
                values: new object[,]
                {
                    { 6, false, "1234567", "epppe@ee.dk" },
                    { 1, true, "admin", "admin" },
                    { 2, false, "user", "user" },
                    { 3, false, "1234567", "ee@ee.dk" },
                    { 4, false, "1234567", "annd@ee.dk" },
                    { 5, false, "1234567", "jdgt@eje.dk" },
                    { 7, false, "1234567", "nvyt@ee.dk" }
                });

            migrationBuilder.InsertData(
                table: "Customers",
                columns: new[] { "Id", "AddressId", "Contactperson", "Cvr", "Email", "IsActive", "IsBusiness", "Name", "Phone" },
                values: new object[,]
                {
                    { 1, 1, "Line Jensen", "6666855545", "abs@hhhcom", true, true, "ABS group", "666667777" },
                    { 2, 2, "Inga Pedersen", "82828282", "factura@embu.dk", true, true, "Emballage Buro, APS", "4545454512" },
                    { 3, 3, "Ivar Nielsen", null, "ivar@fgf.com", true, false, "Ivar Nielsen", "737312909" },
                    { 4, 13, null, null, "maj@fgf.com", true, false, "Maj Hasfeld", "637312909" }
                });

            migrationBuilder.InsertData(
                table: "Employees",
                columns: new[] { "Id", "AddressId", "FirstName", "IsActive", "LastName", "Phone", "Picture", "UIColorCode", "UserId" },
                values: new object[,]
                {
                    { 1, 10, "Anna", true, "Smith", "56432970", null, null, 2 },
                    { 2, 11, "Harpreet", true, "Kaur", "23896714", null, null, 3 },
                    { 3, 12, "Inga", true, "Kalna", "782035478", null, null, 4 },
                    { 4, 9, "Laila", true, "Freiberga", "53896714", null, null, 5 },
                    { 5, 8, "Igo", true, "Neilands", "23886714", null, null, 6 },
                    { 6, 10, "Hugo", true, "Johansons", "23616714", null, null, 7 }
                });

            migrationBuilder.InsertData(
                table: "Projects",
                columns: new[] { "Id", "AddressId", "CustomerId", "HourlyRate", "IsActive", "IsHourlyPaid", "Name", "PricePerPiece", "ProjectDescription", "TaskDescription" },
                values: new object[,]
                {
                    { 1, 4, 1, 420m, true, true, "ABS Reception", null, "Description", "Tasks...." },
                    { 2, 4, 1, 420m, true, true, "ABS Fabfrik", null, "Description", "Tasks...." },
                    { 3, 6, 1, null, true, false, "ABS Hus", 1200m, "Description", "Tasks...." },
                    { 4, 2, 2, 420m, true, true, "EMBU Kontor", null, "Description", "Tasks...." },
                    { 5, 7, 3, null, true, false, "Ivar Nielsen Sommerhus", 950m, "Description", "Tasks...." },
                    { 6, 13, 4, null, true, false, "Maj Hasfeld", 950m, "Description", "Tasks...." }
                });

            migrationBuilder.CreateIndex(
                name: "IX_Customers_AddressId",
                table: "Customers",
                column: "AddressId");

            migrationBuilder.CreateIndex(
                name: "IX_Employees_AddressId",
                table: "Employees",
                column: "AddressId");

            migrationBuilder.CreateIndex(
                name: "IX_Employees_UserId",
                table: "Employees",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_EmployeeSchedule_ScheduledJobsID",
                table: "EmployeeSchedule",
                column: "ScheduledJobsID");

            migrationBuilder.CreateIndex(
                name: "IX_Projects_AddressId",
                table: "Projects",
                column: "AddressId");

            migrationBuilder.CreateIndex(
                name: "IX_Projects_CustomerId",
                table: "Projects",
                column: "CustomerId");

            migrationBuilder.CreateIndex(
                name: "IX_Schedules_CustomerId",
                table: "Schedules",
                column: "CustomerId");

            migrationBuilder.CreateIndex(
                name: "IX_Schedules_ProjectId",
                table: "Schedules",
                column: "ProjectId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "EmployeeSchedule");

            migrationBuilder.DropTable(
                name: "Employees");

            migrationBuilder.DropTable(
                name: "Schedules");

            migrationBuilder.DropTable(
                name: "Users");

            migrationBuilder.DropTable(
                name: "Projects");

            migrationBuilder.DropTable(
                name: "Customers");

            migrationBuilder.DropTable(
                name: "Addresses");
        }
    }
}
