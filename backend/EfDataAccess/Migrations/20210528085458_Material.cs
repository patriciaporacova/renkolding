﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace EfDataAccess.Migrations
{
    public partial class Material : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsDone",
                table: "Schedules",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.CreateTable(
                name: "Materials",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Price = table.Column<decimal>(type: "decimal(18,2)", nullable: true),
                    ScheduleId = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Materials", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Materials_Schedules_ScheduleId",
                        column: x => x.ScheduleId,
                        principalTable: "Schedules",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.InsertData(
                table: "Materials",
                columns: new[] { "Id", "Name", "Price", "ScheduleId" },
                values: new object[] { 1, "Novadan Universal Rens 5L", 160.67m, null });

            migrationBuilder.InsertData(
                table: "Materials",
                columns: new[] { "Id", "Name", "Price", "ScheduleId" },
                values: new object[] { 2, "Unversal klude 1stk", 9.99m, null });

            migrationBuilder.InsertData(
                table: "Materials",
                columns: new[] { "Id", "Name", "Price", "ScheduleId" },
                values: new object[] { 3, "Novadan Toiletrens 1L", 12.99m, null });

            migrationBuilder.CreateIndex(
                name: "IX_Materials_ScheduleId",
                table: "Materials",
                column: "ScheduleId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Materials");

            migrationBuilder.DropColumn(
                name: "IsDone",
                table: "Schedules");
        }
    }
}
