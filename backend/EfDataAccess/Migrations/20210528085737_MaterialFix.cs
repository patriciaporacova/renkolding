﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace EfDataAccess.Migrations
{
    public partial class MaterialFix : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Materials_Schedules_ScheduleId",
                table: "Materials");

            migrationBuilder.DropIndex(
                name: "IX_Materials_ScheduleId",
                table: "Materials");

            migrationBuilder.DropColumn(
                name: "ScheduleId",
                table: "Materials");

            migrationBuilder.CreateTable(
                name: "MaterialSchedule",
                columns: table => new
                {
                    MaterialsId = table.Column<int>(type: "int", nullable: false),
                    SchedulesId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MaterialSchedule", x => new { x.MaterialsId, x.SchedulesId });
                    table.ForeignKey(
                        name: "FK_MaterialSchedule_Materials_MaterialsId",
                        column: x => x.MaterialsId,
                        principalTable: "Materials",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_MaterialSchedule_Schedules_SchedulesId",
                        column: x => x.SchedulesId,
                        principalTable: "Schedules",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_MaterialSchedule_SchedulesId",
                table: "MaterialSchedule",
                column: "SchedulesId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "MaterialSchedule");

            migrationBuilder.AddColumn<int>(
                name: "ScheduleId",
                table: "Materials",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Materials_ScheduleId",
                table: "Materials",
                column: "ScheduleId");

            migrationBuilder.AddForeignKey(
                name: "FK_Materials_Schedules_ScheduleId",
                table: "Materials",
                column: "ScheduleId",
                principalTable: "Schedules",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
