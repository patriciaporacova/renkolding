﻿
using EfDataAccess.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace EfDataAccess
{
    public static class ModelBuilderExtensions
    {

        public static void Seed(this ModelBuilder modelBuilder)
        {

            Address a1 = new Address { Id = 1, AddressLine1 = "Langelinie 34", City = "Kolding", Country = "Danmark", Zip = "6000" };
            Address a2 = new Address { Id = 2, AddressLine1 = "Sommervej 34", City = "Kolding", Country = "Danmark", Zip = "6000" };
            Address a3 = new Address { Id = 3, AddressLine1 = "Vejlevej 67", City = "Kolding", Country = "Danmark", Zip = "6000" };
            Address a4 = new Address { Id = 4, AddressLine1 = "Fynsvej 8", City = "Kolding", Country = "Danmark", Zip = "6000" };
            Address a5 = new Address { Id = 5, AddressLine1 = "Haderslevvej 134", City = "Kolding", Country = "Danmark", Zip = "6000" };
            Address a6 = new Address { Id = 6, AddressLine1 = "Tunøvænget 31", City = "Kolding", Country = "Danmark", Zip = "6000" };
            Address a7 = new Address { Id = 7, AddressLine1 = "Strandvej 3", City = "Kolding", Country = "Danmark", Zip = "6000" };
            Address a8 = new Address { Id = 8, AddressLine1 = "Marsvej 6", City = "Kolding", Country = "Danmark", Zip = "6000" };
            Address a9 = new Address { Id = 9, AddressLine1 = "Platinvej 8", City = "Kolding", Country = "Danmark", Zip = "6000" };
            Address a10 = new Address { Id = 10, AddressLine1 = "Fabrikvej 3", City = "Kolding", Country = "Danmark", Zip = "6000" };
            Address a11 = new Address { Id = 11, AddressLine1 = "Pillevej 4", City = "Kolding", Country = "Danmark", Zip = "6000" };
            Address a12 = new Address { Id = 12, AddressLine1 = "Odensevej 5", City = "Kolding", Country = "Danmark", Zip = "6000" };
            Address a13 = new Address { Id = 13, AddressLine1 = "Tøndervej 75", City = "Kolding", Country = "Danmark", Zip = "6000" };


            Customer c1 = new Customer { Id = 1, Name = "ABS group", Phone = "666667777", AddressId = a1.Id, Contactperson = "Line Jensen", Cvr = "6666855545", Email = "abs@hhhcom", IsActive = true, IsBusiness = true };
            Customer c2 = new Customer { Id = 2, Name = "Emballage Buro, APS", Phone = "4545454512", AddressId = a2.Id, Contactperson = "Inga Pedersen", Cvr = "82828282", Email = "factura@embu.dk", IsActive = true, IsBusiness = true };
            Customer c3 = new Customer { Id = 3, Name = "Ivar Nielsen", Phone = "737312909", AddressId = a3.Id, Contactperson = "Ivar Nielsen", Email = "ivar@fgf.com", IsActive = true, IsBusiness = false };
            Customer c4 = new Customer { Id = 4, Name = "Maj Hasfeld", Phone = "637312909", AddressId = a13.Id,  Email = "maj@fgf.com", IsActive = true, IsBusiness = false };

            Project p1 = new Project { Id = 1, CustomerId = 1, AddressId = 4, Name = "ABS Reception", IsHourlyPaid = true, HourlyRate = 420, IsActive = true, ProjectDescription = "Description", TaskDescription = "Tasks...." };
            Project p2 = new Project { Id = 2, CustomerId = 1, AddressId = 4, Name = "ABS Fabfrik", IsHourlyPaid = true, HourlyRate = 420, IsActive = true, ProjectDescription = "Description", TaskDescription = "Tasks...." };
            Project p3 = new Project { Id = 3, CustomerId = 1, AddressId = 6, Name = "ABS Hus", IsHourlyPaid = false, PricePerPiece = 1200, IsActive = true, ProjectDescription = "Description", TaskDescription = "Tasks...." };
            Project p4 = new Project { Id = 4, CustomerId = 2, AddressId = 2, Name = "EMBU Kontor", IsHourlyPaid = true, HourlyRate = 420, IsActive = true, ProjectDescription = "Description", TaskDescription = "Tasks...." };
            Project p5 = new Project { Id = 5, CustomerId = 3, AddressId = 7, Name = "Ivar Nielsen Sommerhus", IsHourlyPaid = false, PricePerPiece = (decimal?)950.00, IsActive = true, ProjectDescription = "Description", TaskDescription = "Tasks...." };
            Project p6 = new Project { Id = 6, CustomerId = 4, AddressId = 13, Name = "Maj Hasfeld", IsHourlyPaid = false, PricePerPiece = (decimal?)950.00, IsActive = true, ProjectDescription = "Description", TaskDescription = "Tasks...." };

            User u1 = new User { Id = 1, IsAdmin = true, Password = "admin", Username = "admin" };
            User u2 = new User { Id = 2, IsAdmin = false, Password = "user", Username = "user" };
            User u3 = new User { Id = 3, IsAdmin = false, Password = "1234567", Username = "ee@ee.dk" };
            User u4 = new User { Id = 4, IsAdmin = false, Password = "1234567", Username = "annd@ee.dk" };
            User u5 = new User { Id = 5, IsAdmin = false, Password = "1234567", Username = "jdgt@eje.dk" };
            User u6 = new User { Id = 6, IsAdmin = false, Password = "1234567", Username = "epppe@ee.dk" };
            User u7 = new User { Id = 7, IsAdmin = false, Password = "1234567", Username = "nvyt@ee.dk" };

            Employee e1 = new Employee { Id = 1, UserId = 2, AddressId = 10, FirstName = "Anna", LastName = "Smith", Phone = "56432970", IsActive = true };
            Employee e2 = new Employee { Id = 2, UserId = 3, AddressId = 11, FirstName = "Harpreet", LastName = "Kaur", Phone = "23896714", IsActive = true };
            Employee e3 = new Employee { Id = 3, UserId = 4, AddressId = 12, FirstName = "Inga", LastName = "Kalna", Phone = "782035478", IsActive = true };
            Employee e4 = new Employee { Id = 4, UserId = 5, AddressId = 9, FirstName = "Laila", LastName = "Freiberga", Phone = "53896714", IsActive = true };
            Employee e5 = new Employee { Id = 5, UserId = 6, AddressId = 8, FirstName = "Igo", LastName = "Neilands", Phone = "23886714", IsActive = true };
            Employee e6 = new Employee { Id = 6, UserId = 7, AddressId = 10, FirstName = "Hugo", LastName = "Johansons", Phone = "23616714", IsActive = true };

            Material m = new Material { Id = 1, Name = "Novadan Universal Rens 5L", Price = (decimal?)160.67 };
            Material m2 = new Material { Id = 2, Name = "Unversal klude 1stk", Price = (decimal?)9.99 };
            Material m3 = new Material { Id = 3, Name = "Novadan Toiletrens 1L", Price = (decimal?)12.99 };

            modelBuilder.Entity<Customer>().HasData(c1, c2, c3, c4);

            modelBuilder.Entity<Project>().HasData(p1, p2, p3, p4, p5, p6);

            modelBuilder.Entity<Address>().HasData(a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11, a12, a13);

            modelBuilder.Entity<User>().HasData(u1, u2, u3, u4, u5, u6, u7);
            modelBuilder.Entity<Employee>().HasData(e1, e2, e3, e4, e5, e6);
            modelBuilder.Entity<Material>().HasData(m, m2, m3);

        }

        }
}
