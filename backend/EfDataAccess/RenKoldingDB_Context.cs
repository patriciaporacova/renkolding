﻿
using EfDataAccess.Entities;
using Microsoft.EntityFrameworkCore;

namespace EfDataAccess
{
    public class RenKoldingDB_Context : DbContext

    {

        public RenKoldingDB_Context(DbContextOptions options) : base(options) { }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Project> Projects { get; set; }
        public DbSet<Address> Addresses { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Employee> Employees { get; set; }
        public DbSet<Schedule> Schedules { get; set; }
        public DbSet<Material> Materials { get; set; }






        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {     
  
            modelBuilder.Seed();
        }

    }
}