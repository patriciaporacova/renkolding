﻿using Domain.Models;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RenKoldingApi.Controllers;
using RenKoldingApi.Infrastructure;
using Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Mime;
using System.Security.Claims;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using Xunit;

namespace RenKoldingApi.IntegrationTests
{
    [TestClass]
    public class CustomerControllerTest
    {
        private readonly TestHostFixture _testHostFixture = new TestHostFixture();
        private HttpClient _httpClient;
        private IServiceProvider _serviceProvider;


        [TestInitialize]
        public async Task SetUp()
        {
            _httpClient = _testHostFixture.Client;
            _serviceProvider = _testHostFixture.ServiceProvider;

            var credentials = new LoginRequest
            {
                UserName = "admin1",
                Password = "admin1"
            };
            var loginResponse = await _httpClient.PostAsync("api/account/login",
                new StringContent(System.Text.Json.JsonSerializer.Serialize(credentials), Encoding.UTF8,
                    MediaTypeNames.Application.Json));
            var loginResponseContent = await loginResponse.Content.ReadAsStringAsync();
            var loginResult = System.Text.Json.JsonSerializer.Deserialize<LoginResult>(loginResponseContent);

            var jwtAuthManager = _serviceProvider.GetRequiredService<IJwtAuthManager>();
            Assert.IsTrue(jwtAuthManager.UsersRefreshTokensReadOnlyDictionary.ContainsKey(loginResult.RefreshToken));

            _httpClient.DefaultRequestHeaders.Authorization =
                new AuthenticationHeaderValue(JwtBearerDefaults.AuthenticationScheme, loginResult.AccessToken);

        }

        [TestMethod]
        public async Task ShouldExpect200WhenGetAllCustomers()  
        {
           
            var customersResponse =  await _httpClient.GetAsync("api/customer");
                      
            //var responseContent = await customersResponse.Content.ReadAsStringAsync();

            Assert.AreEqual(HttpStatusCode.OK, customersResponse.StatusCode);

        }


        [TestMethod]
        public async Task ShouldExpect201WhenCreateCustomer()
        {
            CustomerDTO c = new()
            {
                Name = "ABC Sales",
                Phone = "777777777",
                IsBusiness = true,
                Cvr = "7854128876",
                IsActive = true,
                Email = "grthh@taoh9o9.com",
                Contactperson = "Tanja"
                
            };

            var response = await _httpClient.PostAsync("api/customer",
                 new StringContent(JsonSerializer.Serialize(c), Encoding.UTF8,
                     MediaTypeNames.Application.Json));
                     

            Assert.AreEqual(HttpStatusCode.Created, response.StatusCode);

        }

    }
}
