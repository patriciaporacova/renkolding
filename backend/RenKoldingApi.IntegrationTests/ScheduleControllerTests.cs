﻿using Domain.Models;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using RenKoldingApi.Controllers;
using RenKoldingApi.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Mime;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace RenKoldingApi.IntegrationTests
{
    [TestClass]
    public class ScheduleControllerTests
    {


        private readonly TestHostFixture _testHostFixture = new TestHostFixture();
        private HttpClient _httpClient;
        private IServiceProvider _serviceProvider;


        [TestInitialize]
        public async Task SetUp()
        {
            _httpClient = _testHostFixture.Client;
            _serviceProvider = _testHostFixture.ServiceProvider;

            var credentials = new LoginRequest
            {
                UserName = "admin1",
                Password = "admin1"
            };
            var loginResponse = await _httpClient.PostAsync("api/account/login",
                new StringContent(System.Text.Json.JsonSerializer.Serialize(credentials), Encoding.UTF8,
                    MediaTypeNames.Application.Json));
            var loginResponseContent = await loginResponse.Content.ReadAsStringAsync();
            var loginResult = System.Text.Json.JsonSerializer.Deserialize<LoginResult>(loginResponseContent);

            var jwtAuthManager = _serviceProvider.GetRequiredService<IJwtAuthManager>();
            Assert.IsTrue(jwtAuthManager.UsersRefreshTokensReadOnlyDictionary.ContainsKey(loginResult.RefreshToken));

            _httpClient.DefaultRequestHeaders.Authorization =
                new AuthenticationHeaderValue(JwtBearerDefaults.AuthenticationScheme, loginResult.AccessToken);

        }


        [TestMethod]
        public void ShouldExpect_200_WhenGetCurrentMonthSchedule()
        {
            var customersResponse = _httpClient.GetAsync("api/Schedule/current-month").Result;
            Assert.AreEqual(HttpStatusCode.OK, customersResponse.StatusCode);

        }

        [TestMethod]
        public void ShouldExpect_200_WhenGetScheduleByDates()
        {
            var response = _httpClient.GetAsync("api/Schedule/2021-05-01/2021-05-31").Result;
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);

        }

        [TestMethod]
        public async Task ShouldExpect_201_WhenScheduleCreated()
        {
            ScheduleDTO scheduleDTO = new()
            {
                WorkingDate = new DateTime(2021 - 07 - 31),
                StartTime = "8:00",
                EndTime = "12:00",
                TodayTasks = "",
                CustomerId = 2,
                ProjectId = 1,
                IsDone = false,
                EmployeeIds = new int[] { 3, 4 }
            };

            var response = await _httpClient.PostAsync("api/schedule",
            new StringContent(System.Text.Json.JsonSerializer.Serialize(scheduleDTO), Encoding.UTF8,
                MediaTypeNames.Application.Json));

            Assert.AreEqual(HttpStatusCode.Created, response.StatusCode);
        }

        [TestMethod]
        public void ShouldExpect_404_WhenJobNotFound()
        {

            var response = _httpClient.GetAsync("api/Schedule/notValidID").Result;
            Assert.AreEqual(HttpStatusCode.NotFound, response.StatusCode);

        }

        [TestMethod]
        public async Task ShouldExpect_200_SetJobAsDone()
        {
            var response = await _httpClient.PutAsync("api/Schedule/isDone/520", null);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);



        }
    }
}
