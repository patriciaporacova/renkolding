﻿using Domain.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Repositories.Repositories;
using System;
using System.Threading.Tasks;

namespace RenKoldingApi.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    public class AddressController : Controller
    {
        private readonly IAddressRepository _addressRepository;

        public AddressController(IAddressRepository addressRepository)
        {
            _addressRepository = addressRepository;
        }

        // POST: AddressController/Create
        [Authorize]
        [HttpPost]
        public async Task<ActionResult<AddressDTO>> Create(AddressDTO addressDTO)
        {
            try
            {

                if (addressDTO == null)
                {
                    return BadRequest();
                }


                var createdAddress = await _addressRepository.AddAddress(addressDTO);

                return CreatedAtAction(nameof(GetAddress), new { id = addressDTO.Id },
                        createdAddress);


            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError,
                    "Error creating customer");
            }
        }

        [Authorize]
        [HttpGet("{id:int}")]
        public async Task<ActionResult<AddressDTO>> GetAddress(int id)
        {
            try
            {
                var result = await _addressRepository.GetAddress(id);

                if (result == null)
                {
                    return NotFound();
                }

                return result;
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError,
                    "Error retrieving data from the database");
            }
        }


    }
}
