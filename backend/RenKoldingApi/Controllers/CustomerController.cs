﻿using Microsoft.AspNetCore.Mvc;
using Repositories;
using System.Threading.Tasks;
using Domain.Models;
using System;
using Microsoft.AspNetCore.Http;
using Repositories.Repositories;
using Microsoft.AspNetCore.Authorization;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace RenKoldingApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CustomerController : ControllerBase
    {


        private readonly ICustomerRepository _customerRepository;
        private readonly IAddressRepository _addressRepository;

        public CustomerController(ICustomerRepository customerRepository, IAddressRepository addressRepository)
        {
            _customerRepository = customerRepository;
            _addressRepository = addressRepository;
        }


        // GET: api/<CustomerController>
        [Authorize]
        [HttpGet]
        public async Task<ActionResult> GetAllCustomers()
        {
            try
            {
                var customers = await _customerRepository.GetAllCustomers();
                return Ok(customers);
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError,
                    "Error retrieving data from the database");
            }

        }

        // GET: api/<CustomerController>
        [Authorize]
        [HttpGet("all-active-customers")]
        public async Task<ActionResult> GetAllActiveCustomers()
        {
            try
            {
                return Ok(await _customerRepository.GetAllActiveCustomers());
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError,
                    "Error retrieving data from the database");
            }


        }

        [Authorize]
        [HttpPost]
        public async Task<ActionResult<CustomerDTO>> CreateCustomer(CustomerDTO customer)
        {

            try
            {

                if (customer == null)
                {
                    return BadRequest();
                }


                var isEmailAvailable = await _customerRepository.IsEmailAvailable(customer.Email);

                if (isEmailAvailable == false)
                {
                    return Conflict($"Email {customer.Email} is already in use.");
                }

                var isCvrAvailable = await _customerRepository.IsCvrAvailable(customer.Cvr);

                if (isCvrAvailable == false)
                {
                    return Conflict($"CVR {customer.Cvr} is already in use.");
                }


                var createdCustomer = await _customerRepository.AddCustomer(customer);

                return CreatedAtAction(nameof(GetCustomer), new { id = createdCustomer.Id },
                        createdCustomer);


            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError,
                    "Error creating customer");
            }
        }

        [Authorize]
        [HttpPut]
        public async Task<ActionResult> UpdateCustomer(CustomerDTO customer)
        {

            try
            {
                var customerToUpdate = await _customerRepository.GetCustomer(customer.Id);

                if (customerToUpdate == null)
                    return NotFound($"Customer with Id = {customer.Id} not found");

                await _customerRepository.UpdateCustomer(customer);
                return Ok();
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError,
                    "Error updating data");
            }

        }

        [Authorize]
        [HttpGet("{id:int}")]
        public async Task<ActionResult<CustomerDTO>> GetCustomer(int id)
        {
            try
            {
                var result = await _customerRepository.GetCustomer(id);

                if (result == null)
                {
                    return NotFound();
                }

                return result;
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError,
                    "Error retrieving data from the database");
            }
        }

        [Authorize]
        [HttpDelete("{id:int}")]
        public async Task<ActionResult> DeleteCustomer(int id)
        {
            try
            {
                var customerToDelete = await _customerRepository.GetCustomer(id);

                if (customerToDelete == null)
                {
                    return NotFound($"Customer with Id = {id} not found");
                }

                await _customerRepository.DeleteCustomer(id);
                return Ok();
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError,
                    "Error deleting data");
            }
        }

        [Authorize]
        [HttpPut("newaddress")]
        public async Task<ActionResult> AssignNewAddress(int customerId, int addresId)
        {

            try
            {
                var customerToUpdate = await _customerRepository.GetCustomer(customerId);
                var newAddress = await _addressRepository.GetAddress(addresId);


                if (customerToUpdate == null)
                    return NotFound($"Employee with Id = {customerId} not found");
                if (newAddress == null)
                    return NotFound($"Address with Id = {addresId} not found");

                await _customerRepository.AssignNewAddress(customerId, addresId);
                return Ok();
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError,
                    "Error updating data");
            }

        }

        [Authorize]
        [HttpPut("suspendCustomer/{id:int}")]
        public async Task<ActionResult> SuspendCustomer(int id)
        {

            try
            {
                var customerToSuspend = await _customerRepository.GetCustomer(id);

                if (customerToSuspend == null)
                    return NotFound($"Customer with Id = {id} not found");

                await _customerRepository.SuspendCustomer(id);
                return Ok();
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError,
                    "Error updating data");
            }

        }

    }
}
