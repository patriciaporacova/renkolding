﻿using Domain.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Repositories;
using Repositories.Repositories;
using System;
using System.Threading.Tasks;

namespace RenKoldingApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmployeeController : ControllerBase
    {
        private readonly IEmployeeRepository _employeeRepository;
        private readonly IUserRepository _userRepository;
        private readonly IAddressRepository _addressRepositiry;



        public EmployeeController(IEmployeeRepository employeeRepository,
            IAddressRepository addressRepositiry, IUserRepository userRepository)
        {
            _employeeRepository = employeeRepository;
            _userRepository = userRepository;
            _addressRepositiry = addressRepositiry;

        }

        // GET: api/<EmployeeController>
        [Authorize]
        [HttpGet]
        public async Task<ActionResult> GetAllEmployees()
        {
            try
            {
                return Ok(await _employeeRepository.GetAllEmployees());
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError,
                    "Error retrieving data from the database");
            }

        }

        [Authorize]
        [HttpGet("all-active-employees")]
        public async Task<ActionResult> GetAllActiveEmployees()
        {
            try
            {
                return Ok(await _employeeRepository.GetAllActiveEmployees());
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError,
                    "Error retrieving data from the database");
            }


        }

        [Authorize]
        [HttpPost]
        public async Task<ActionResult> CreateEmployee(EmployeeDTO employeeDTO, string username, string password, bool isAdmin)
        {
            try
            {
                if (employeeDTO == null)
                {
                    return BadRequest();
                }
                var userDTO = new UserDTO
                {
                    Username = username,
                    Password = password,
                    IsAdmin = isAdmin
                };

                var isEmailAvailable = await _userRepository.IsEmailAvailable(username);

                if (isEmailAvailable == false)
                {
                    return Conflict($"Email {username} is already in use.");
                }

                var createdEmployee = await _employeeRepository.CreateEmployee(employeeDTO, userDTO);

                return CreatedAtAction(nameof(GetEmployee), new { id = createdEmployee.Id },
                    createdEmployee);
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError,
                    "Error creating customer");
            }
        }

        [Authorize]
        [HttpPut("{id:int}")]
        public async Task<ActionResult> UpdateEmployee(int id, EmployeeDTO employeeDTO)
        {

            try
            {
                if (id != employeeDTO.Id)
                    return BadRequest("Employee ID mismatch");

                var customerToUpdate = await _employeeRepository.GetEmployee(employeeDTO.Id);

                if (customerToUpdate == null)
                    return NotFound($"Employee with Id = {employeeDTO.Id} not found");

                await _employeeRepository.UpdateEmployee(employeeDTO);
                return Ok();
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError,
                    "Error updating data");
            }

        }

        [Authorize]
        [HttpPut("newaddress/{employeeId:int}")]
        public async Task<ActionResult> AssignNewAddress(int employeeId, int addresId)
        {

            try
            {                
                var customerToUpdate = await _employeeRepository.GetEmployee(employeeId);
                var newAddress = await _addressRepositiry.GetAddress(addresId);
               

                if (customerToUpdate == null)
                    return NotFound($"Employee with Id = {employeeId} not found");
                if (newAddress == null)
                    return NotFound($"Address with Id = {addresId} not found");

                await _employeeRepository.AssignNewAddress(employeeId, addresId);
                return Ok();
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError,
                    "Error updating data");
            }

        }

        [Authorize]
        [HttpGet("{id:int}")]
        public async Task<ActionResult<EmployeeDTO>> GetEmployee(int id)
        {
            try
            {
                var result = await _employeeRepository.GetEmployee(id);

                if (result == null)
                {
                    return NotFound();
                }

                return result;
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError,
                    "Error retrieving data from the database");
            }
        }

        [Authorize]
        [HttpGet("user/{userId:int}")]
        public async Task<ActionResult<EmployeeDTO>> GetEmployeeByUserId(int userId)
        {
            try
            {
                var result = await _employeeRepository.GetEmployeeByUserId(userId);

                if (result == null)
                {
                    return NotFound();
                }

                return result;
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError,
                    "Error retrieving data from the database");
            }
        }

        [Authorize]
        [HttpDelete("{id:int}")]
        public async Task<ActionResult> DeleteEmployee(int id)
        {
            try
            {
                var employeeToDelete = await _employeeRepository.GetEmployee(id);

                if (employeeToDelete == null)
                {
                    return NotFound($"Employee with Id = {id} not found");
                }

                 await _employeeRepository.DeleteEmployee(id);
                return Ok();
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError,
                    "Error deleting data");
            }
        }

        [Authorize]
        [HttpGet("hours/{employeeId:int}")]
        public async Task<ActionResult> GetHours(DateTime startDate, DateTime endDate, int employeeId)
        {
            try
            {
                var duration = await _employeeRepository.GetHoursByEmployee(startDate, endDate, employeeId);
                return Ok(duration);
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError,
                    "Error retrieving data from the database");
            }

        }

        [Authorize]
        [HttpPut("suspend/{id:int}")]
        public async Task<ActionResult> SuspendEmployee(int id)
        {

            try
            {
                var employeeToSuspend = await _employeeRepository.GetEmployee(id);

                if (employeeToSuspend == null)
                    return NotFound($"Employee with Id = {id} not found");

                await _employeeRepository.SuspendEmployee(id);
                return Ok();
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError,
                    "Error updating data");
            }

        }

    }
}
