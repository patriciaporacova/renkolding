﻿using Microsoft.AspNetCore.Mvc;
using Repositories.Repositories;
using System;
using Domain.Models;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Authorization;

namespace RenKoldingApi.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    public class MaterialController : Controller


    {

        private readonly IMaterialRepository _materialRepository;

        public MaterialController(IMaterialRepository materialRepository)
        {
            _materialRepository = materialRepository;

        }

        [Authorize]
        [HttpPost]
        public async Task<ActionResult<MaterialDTO>> CreateMaterial(MaterialDTO materialDTO)
        {

            try
            {

                if (materialDTO == null)
                {
                    return BadRequest();
                }


                var createdMaterial = await _materialRepository.AddMaterial(materialDTO);

                return CreatedAtAction(nameof(GetMaterial), new { id = createdMaterial.Id },
                        createdMaterial);


            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError,
                    "Error creating material");
            }
        }

        [Authorize]
        [HttpGet]
        public async Task<ActionResult> GetAllMaterials()
        {
            try
            {
                var materials = await _materialRepository.GetAllMaterials();
                return Ok(materials);
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError,
                    "Error retrieving data from the database");
            }

        }

        [Authorize]
        [HttpGet("{id:int}")]
        public async Task<ActionResult<MaterialDTO>> GetMaterial(int id)
        {
            try
            {
                var result = await _materialRepository.GetMaterial(id);

                if (result == null)
                {
                    return NotFound();
                }

                return Ok(result);
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError,
                    "Error retrieving data from the database");
            }
        }

        [Authorize]
        [HttpGet("usedInJob/{jobId:int}")]
        public async Task<ActionResult> GetMaterialsByJob(int jobId)
        {
            try
            {
                var result = await _materialRepository.GetMaterialsByJob(jobId);

                if (result == null)
                {
                    return NotFound();
                }

                return Ok(result);
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError,
                    "Error retrieving data from the database");
            }
        }


    }
}
