﻿using Domain.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Repositories;
using Repositories.Repositories;
using System;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace RenKoldingApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProjectController : ControllerBase
    {

        private readonly IProjectRepository _projectRepository;
        private readonly IAddressRepository _addressRepository;

        public ProjectController(IProjectRepository projectRepository,  IAddressRepository addressRepository)
        {
            _projectRepository = projectRepository;
            _addressRepository = addressRepository;

    }

        //[Authorize]
        [HttpGet("{customerId:int}")]
        public async Task<ActionResult> GetProjectsByCustomerId(int customerId)
        {
            try
            {
                return Ok(await _projectRepository.GetProjectsByCustomerId(customerId));
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError,
                    "Error retrieving data from the database");
            }

        }

        //[Authorize]
        [HttpPost("{customerId:int}")]
        public async Task<ActionResult> CreateProject(int customerId, ProjectDTO project)
        {
            try
            {
                if (project == null)
                {
                    return BadRequest();
                }
              
                project.CustomerId = customerId;
                var createdProject = await _projectRepository.AddProject(project);

                return CreatedAtAction(nameof(GetProjectById), new { id = createdProject.Id },
             createdProject);


            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError,
                    "Error creating customer");
            }

        }

        [HttpDelete("{id:int}")]
        public async Task<ActionResult> DeleteProject(int id)
        {
            try
            {
                var projectToDelete = await _projectRepository.GetProject(id);

                if (projectToDelete == null)
                {
                    return NotFound($"Project with Id = {id} not found");
                }

                await _projectRepository.DeleteProject(id);
                return Ok();
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError,
                    "Error deleting data");
            }
        }

        //[Authorize]
        [HttpGet]
        public async Task<ActionResult<ProjectDTO>> GetProjectById(int projectId)
        {
            try
            {
                var result = await _projectRepository.GetProject(projectId);

                if (result == null)
                {
                    return NotFound();
                }

                return result;
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError,
                    "Error retrieving data from the database");
            }
        }

        //[Authorize]
        [HttpPut]
        public async Task<ActionResult> UpdateProject(ProjectDTO projectDTO)
        {

            try
            {
                var projectToUpdate = await _projectRepository.GetProject(projectDTO.Id);

                if (projectToUpdate == null)
                    return NotFound($"Customer with Id = {projectDTO.Id} not found");

                await _projectRepository.UpdateProject(projectDTO);
                return Ok();
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError,
                    "Error updating data");
            }

        }

        //[Authorize]
        [HttpPut("newaddress")]
        public async Task<ActionResult> AssignNewAddress(int projectId, int addresId)
        {

            try
            {
                var customerToUpdate = await _projectRepository.GetProject(projectId);
                var newAddress = await _addressRepository.GetAddress(addresId);


                if (customerToUpdate == null)
                    return NotFound($"Project with Id = {projectId} not found");
                if (newAddress == null)
                    return NotFound($"Address with Id = {addresId} not found");

                await _projectRepository.AssignNewAddress(projectId, addresId);
                return Ok();
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError,
                    "Error updating data");
            }

        }

        //[Authorize]
        [HttpPut("suspendProject/{id:int}")]
        public async Task<ActionResult> SuspendProject(int id)
        {

            try
            {
                var projectToSuspend = await _projectRepository.GetProject(id);

                if (projectToSuspend == null)
                    return NotFound($"Project with Id = {id} not found");

                await _projectRepository.SuspendProject(id);
                return Ok();
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError,
                    "Error updating data");
            }

        }

    }
}
