﻿using Domain.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Repositories;
using System;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace RenKoldingApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ScheduleController : ControllerBase
    {
        private readonly IScheduleRepository _scheduleRepository;

        public ScheduleController(IScheduleRepository scheduleRepository)
        {
            _scheduleRepository = scheduleRepository;
        }

        [Authorize]
        [HttpPost]
        public async Task<ActionResult<ScheduleDTO>> CreateScheduledJob(ScheduleDTO scheduleDTO)
        {
            try
            {

                if (scheduleDTO == null)
                {
                    return BadRequest();
                }
                var createdJob = await _scheduleRepository.AddJob(scheduleDTO);

                return CreatedAtAction(nameof(GetScheduledJob), new { id = createdJob.Id },
                    createdJob);
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError,
                    "Error creating scheduled job");
            }
        }

        [Authorize]
        [HttpPost("{repeat:int}/{endRepeatDate:DateTime}")]
        public async Task<ActionResult> CreateWithRepeat(ScheduleDTO scheduleDTO, int repeat, DateTime endRepeatDate)
        {
            try
            {

                if (scheduleDTO == null)
                {
                    return BadRequest();
                }

                var create = await _scheduleRepository.AddJobRepeat(scheduleDTO, repeat, endRepeatDate);
                return StatusCode(201, create);
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError,
                    "Error creating scheduled job");
            }
        }


        [Authorize]
        [HttpGet("{startDate:DateTime}/{endDate:DateTime}")]
        public async Task<ActionResult> GetSchedule(DateTime startDate, DateTime endDate)
        {
            try
            {
                return Ok(await _scheduleRepository.GetSchedule(startDate, endDate));
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError,
                    "Error retrieving data from the database");
            }

        }

        [Authorize]
        [HttpGet("current-month")]
        public async Task<ActionResult> GetCurrentMonthSchedule()
        {
            try
            {
                return Ok(await _scheduleRepository.GetCurrentMonthSchedule());
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError,
                    "Error retrieving data from the database");
            }

        }

        [Authorize]
        [HttpGet("{id:int}")]
        public async Task<ActionResult<ScheduleDTO>> GetScheduledJob(int id)
        {
            try
            {
                var result = await _scheduleRepository.GetJob(id);

                if (result == null)
                {
                    return NotFound();
                }

                return result;
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError,
                    "Error retrieving data from the database");
            }
        }

        [Authorize]
        [HttpGet("{startDate:DateTime}/{endDate:DateTime}/{employeeId:int}")]
        public async Task<ActionResult> GetScheduleByEmployee(DateTime startDate, DateTime endDate, int employeeId)
        {
            try
            {
                return Ok(await _scheduleRepository.GetByEmployee(startDate, endDate, employeeId));
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError,
                    "Error retrieving data from the database");
            }

        }

        [Authorize]
        [HttpPut("assignEmployee/{employeeId:int}/{jobId:int}")]
        public async Task<ActionResult> AssignEmployees(int employeeId, int jobId)
        {
            try
            {
                await _scheduleRepository.AssignEmployee(employeeId, jobId);

                return Ok();

            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError,
                    "Error retrieving data from the database");
            }
        }

        [Authorize]
        [HttpPut]
        public async Task<ActionResult> UpdateJob(ScheduleDTO jobDTO)
        {

            try
            {
                var jobToUpdate = await _scheduleRepository.GetJob(jobDTO.Id);

                if (jobToUpdate == null)
                    return NotFound($"Scheduled job with Id = {jobDTO.Id} not found");

                await _scheduleRepository.UpdateJOb(jobDTO);
                return Ok();
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError,
                    "Error updating data");
            }

        }

        [Authorize]
        [HttpPut("usedMaterial/{jobId:int}/{materialId:int}")]
        public async Task<ActionResult> AddMaterialToJob(int jobId, int materialId)
        {
            try
            {
                await _scheduleRepository.AddMaterialToJob(jobId, materialId);

                return Ok();

            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError,
                    "Error retrieving data from the database");
            }
        }

        [Authorize]
        [HttpPut("isDone/{jobId:int}")]
        public async Task<ActionResult> SetJobAsDone(int jobId)
        {
            try
            {
                await _scheduleRepository.SetJobAsDone(jobId);

                return Ok();

            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError,
                    "Error updating data");
            }
        }

        [Authorize]
        [HttpDelete]
        public async Task<ActionResult> DeleteJob(int id)
        {
            try
            {
                var jobToDelete = await _scheduleRepository.GetJob(id);

                if (jobToDelete == null)
                {
                    return NotFound($"Job with Id = {id} not found");
                }

                await _scheduleRepository.DeleteJob(id);
                return Ok();
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError,
                    "Error deleting data");
            }
        }


    }
}
