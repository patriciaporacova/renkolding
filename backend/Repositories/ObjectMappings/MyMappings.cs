﻿using AutoMapper;
using Domain.Models;
using EfDataAccess.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Repositories
{
    public class MyMappings : Profile
    {

        public MyMappings()
        {
            CreateMap<CustomerDTO, Customer>().ReverseMap();
            CreateMap<Customer, CustomerDTO>();
            CreateMap<MaterialDTO, Material>().ReverseMap();
            CreateMap<ProjectDTO, Project>().ReverseMap();
            CreateMap<Project, ProjectDTO>();
            CreateMap<AddressDTO, Address>().ReverseMap();
            CreateMap<EmployeeDTO, Employee>().ReverseMap();
            CreateMap<ScheduleDTO, Schedule>()            
                .ForMember(dest => dest.CustomerId, opt => opt.MapFrom(src => src.CustomerId ))
                .ForMember(dest => dest.ProjectId, opt => opt.MapFrom(src => src.ProjectId))
                .ForMember(dest => dest.StartTime,  opt => opt.MapFrom(src => TimeSpan.Parse(src.StartTime)))
                .ForMember(dest => dest.EndTime, opt => opt.MapFrom(src => TimeSpan.Parse(src.EndTime)))
                .ReverseMap();            
            CreateMap<Schedule, ScheduleDTO>()
                .ForMember(d => d.EmployeeIds, o => o.MapFrom(s => s.Employees.Select(c => c.Id)
                .ToArray()));

            CreateMap<UserDTO, User>().ReverseMap();

           

        }  
    }
}
