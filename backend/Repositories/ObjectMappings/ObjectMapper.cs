﻿using AutoMapper;
using Domain.Models;
using EfDataAccess.Entities;
using System;
using System.Collections.Generic;
using System.Text;

// using-automapper-in-a-net-core-class-library
// https://www.abhith.net/blog/using-automapper-in-a-net-core-class-library/
//reference 10


namespace Repositories
{
    class ObjectMapper
    {
        private static readonly Lazy<IMapper> Lazy = new Lazy<IMapper>(() =>
        {
            var config = new MapperConfiguration(cfg =>
            {
               
                cfg.ShouldMapProperty = p => p.GetMethod.IsPublic || p.GetMethod.IsAssembly;
                cfg.AddProfile<MyMappings>();
            });
            var mapper = config.CreateMapper();
            return mapper;
        });
        public static IMapper Mapper
        {
            get
            {
                return Lazy.Value;
            }
        }
    }
}
