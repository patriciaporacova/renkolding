﻿




namespace Repositories.Password

{   // Sources of information:
    // https://medium.com/dealeron-dev/storing-passwords-in-net-core-3de29a3da4d2
    //Reference Nr.2

    public sealed class HashingOptions
    {
        public int Iterations { get; set; } = 10000;
    }
}
