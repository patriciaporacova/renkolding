﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Repositories.Password

// Sources of information:
// https://medium.com/dealeron-dev/storing-passwords-in-net-core-3de29a3da4d2
//Reference Nr.2

{
    public interface IPasswordHasher
    {
        string Hash(string password);

        bool Check(string hash, string password);
    }
}
