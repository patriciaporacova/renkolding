﻿using Domain.Models;
using EfDataAccess;
using EfDataAccess.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Repositories.Repositories
{
    public class AddressRepository : IAddressRepository
    {
        protected readonly RenKoldingDB_Context _context;


        public AddressRepository(RenKoldingDB_Context context)
        {
            _context = context;
        }

        public async Task<AddressDTO> AddAddress(AddressDTO addressDTO)
        {
            var dest = ObjectMapper.Mapper.Map<Address>(addressDTO);
            var result = await _context.Addresses.AddAsync(dest);
            await _context.SaveChangesAsync();
            var createdAddress= ObjectMapper.Mapper.Map<AddressDTO>(result.Entity);
            return createdAddress;
        }

        public async Task<AddressDTO> GetAddress(int id)
        {
            var address = await _context.Addresses.FirstOrDefaultAsync(x => x.Id == id);

            var addressDTO = ObjectMapper.Mapper.Map<AddressDTO>(address);

            return addressDTO;
        }
    }
}
