﻿using AutoMapper;
using Domain.Models;
using EfDataAccess;
using EfDataAccess.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Repositories
{
    public class CustomerRepository : ICustomerRepository
    {
        protected readonly RenKoldingDB_Context _context;


        public CustomerRepository(RenKoldingDB_Context context)
        {
            _context = context;
        }

        public async Task<CustomerDTO> AddCustomer(CustomerDTO customerDTO)
        {
            var dest = ObjectMapper.Mapper.Map<Customer>(customerDTO);
            var result = await _context.Customers.AddAsync(dest);
            await _context.SaveChangesAsync();
            var custDTO = ObjectMapper.Mapper.Map<CustomerDTO>(result.Entity);
            return custDTO;
        }

        public async Task DeleteCustomer(int id)
        {
            var customerToDelete = await _context.Customers.FirstOrDefaultAsync(a => a.Id == id);
            var futureScheduledJobs = await _context.Schedules
                                .Where(x => x.Customer ==customerToDelete && x.WorkingDate>DateTime.Now)
                                .ToListAsync();

            if (customerToDelete.Projects.Count == 0)
            {
                _context.Customers.Remove(customerToDelete);
            }
            else
            {
                customerToDelete.IsActive = false;
                _context.Schedules.RemoveRange(futureScheduledJobs);
                foreach (var project in customerToDelete.Projects)
                {
                    project.IsActive = false;
                }
               
            }
           
            await _context.SaveChangesAsync();
        }

        public async Task<IList<CustomerDTO>> GetAllActiveCustomers()
        {
            var customers = await _context.Customers.
                Where(x => x.IsActive == true).
                Include(x => x.Address).
                Include(x => x.Projects).
                ThenInclude(x => x.Address).
                ToListAsync();

            List<CustomerDTO> customersDto = ObjectMapper.Mapper.Map<List<CustomerDTO>>(customers);

            return customersDto;
        }

        public async Task<IList<CustomerDTO>> GetAllCustomers()
        {

            var customers = await _context.Customers.
               Where(x => x.IsActive == true).
               Include(x => x.Address).
               Include(x => x.Projects).
               ThenInclude(x => x.Address).
                ToListAsync();

            List<CustomerDTO> customersDto = ObjectMapper.Mapper.Map<List<CustomerDTO>>(customers);

            return customersDto;
        }

        public async Task<CustomerDTO> GetCustomer(int id)
        {
            var customer = await _context.Customers
                .Include(x => x.Address)
                .Include(x => x.Projects)
                .ThenInclude(x => x.Address)
                .FirstOrDefaultAsync(x => x.Id == id);

            CustomerDTO custDto = ObjectMapper.Mapper.Map<CustomerDTO>(customer);

            return custDto;


        }

        public async Task<bool> IsEmailAvailable(string email)
        {
            var isAv = await _context.Customers.AllAsync(u =>
                    !u.Email.Equals(email)//ignores case by default
                );

            return isAv;
        }

        public async Task<bool> IsCvrAvailable(string cvr)
        {
            if (cvr != null)
            {
                return await _context.Customers.AllAsync(u =>
                       !u.Cvr.Equals(cvr)

                );
            }
            else return true;
        }

        public async Task UpdateCustomer(CustomerDTO customerDTO)
        {
         
            var result = await _context.Customers
                .FirstOrDefaultAsync(e => e.Id == customerDTO.Id);
                    

            if (result != null)
            {
             
               _context.Entry(result).CurrentValues.SetValues(customerDTO);
               await _context.SaveChangesAsync();
            }

      
        }

        public async Task AssignNewAddress(int customerId, int addresId)
        {
            var customer = await _context.Customers.FirstOrDefaultAsync(e => e.Id == customerId);

            customer.AddressId = addresId;
            await _context.SaveChangesAsync();
        }

        public async Task SuspendCustomer(int id)
        {
            var custstomerToSuspend = await _context.Customers.FirstOrDefaultAsync(e => e.Id == id);

            var futureJobs = await _context.Schedules
           .Where(x => x.Customer.Id == custstomerToSuspend.Id && x.WorkingDate > DateTime.Now)
           .ToListAsync();


            if (custstomerToSuspend.IsActive)
            {
                custstomerToSuspend.IsActive = false;

                foreach (var project in custstomerToSuspend.Projects)
                {
                    project.IsActive = false;
                }
                _context.Schedules.RemoveRange(futureJobs);

            }
            else
            {
                custstomerToSuspend.IsActive = true;
            }


            await _context.SaveChangesAsync();
        }
    }

}
