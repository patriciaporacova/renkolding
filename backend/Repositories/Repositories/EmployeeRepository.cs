﻿using EfDataAccess;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Domain.Models;
using System.Linq;
using EfDataAccess.Entities;
using System;
using Repositories.Password;

namespace Repositories
{
    public class EmployeeRepository : IEmployeeRepository
    {

        protected readonly RenKoldingDB_Context _context;
        private readonly IPasswordHasher _paswordHasher;

        public EmployeeRepository(RenKoldingDB_Context context, IPasswordHasher passwordHasher)
        {
            _context = context;
            _paswordHasher = passwordHasher;
        }

        public async Task AssignNewAddress(int employeeId, int addresId)
        {
            var employeee = await _context.Employees.FirstOrDefaultAsync(e => e.Id == employeeId);
           
                    employeee.AddressId = addresId;
                    await _context.SaveChangesAsync();
                
        }

        public async Task<EmployeeDTO> CreateEmployee(EmployeeDTO employeeDTO, UserDTO userDTO)
        {
            try
            {


                var employee = ObjectMapper.Mapper.Map<Employee>(employeeDTO);
                employee.User = new User
                {
                    Password = _paswordHasher.Hash(userDTO.Password),
                    Username = userDTO.Username
                };
                var result = await _context.Employees.AddAsync(employee);
                await _context.SaveChangesAsync();
                return ObjectMapper.Mapper.Map<EmployeeDTO>(result.Entity);
            }
            catch (Exception e)
            {

                throw;
            }

        }

        public async Task DeleteEmployee(int id)
        {
            var employeeTodelete = await _context.Employees.FirstOrDefaultAsync(e => e.Id == id);
            var userTodelete = await _context.Users.FirstOrDefaultAsync(x => x.Id == employeeTodelete.UserId);

            var hadWorked = await _context.Schedules
          .Where(x => x.Employees.Contains(employeeTodelete))
          .ToListAsync();

          var futureJobs = await _context.Schedules
         .Where(x => x.Employees.Contains(employeeTodelete) && x.WorkingDate >DateTime.Now)
         .Include(x => x.Employees)
         .ToListAsync();

            if (hadWorked.Count == 0)
            {
                _context.Employees.Remove(employeeTodelete);
                _context.Users.Remove(userTodelete);
            }
            else
            {
                employeeTodelete.IsActive = false;
                foreach (var item in futureJobs)
                {
                    item.Employees.Remove(employeeTodelete);
                }
            }
                    
            await _context.SaveChangesAsync();

        }

        public async Task<IList<EmployeeDTO>> GetAllActiveEmployees()
        {
            var employees = await _context.Employees
               .Where(x => x.IsActive == true)
               .Include(x => x.Address)
               .Include(x => x.User)
               .ToListAsync();

            List<EmployeeDTO> employeesDTO = ObjectMapper.Mapper.Map<List<EmployeeDTO>>(employees);

            return employeesDTO;
        }

        public async Task<IList<EmployeeDTO>> GetAllEmployees()
        {
            var employees = await _context.Employees
                .Include(x => x.Address)
                .Include(x => x.User)
                .ToListAsync();

            List<EmployeeDTO> employeesDTO = ObjectMapper.Mapper.Map<List<EmployeeDTO>>(employees);

            return employeesDTO;
        }

        public async Task<EmployeeDTO> GetEmployee(int id)
        {
            var employee = await _context.Employees
                 .Include(nameof(User))
                 .Include(x => x.Address)
                 .FirstOrDefaultAsync(x => x.Id == id);

            EmployeeDTO employeeDTO = ObjectMapper.Mapper.Map<EmployeeDTO>(employee);

            return employeeDTO;
        }

        public async Task UpdateEmployee(EmployeeDTO employeeDTO)
        {
            var employee = ObjectMapper.Mapper.Map<Employee>(employeeDTO);
            var result = await _context.Employees
                .FirstOrDefaultAsync(e => e.Id == employeeDTO.Id);
          

            if (result != null)
            {
                _context.Entry(result).CurrentValues.SetValues(employeeDTO);
       
            }
   
            await _context.SaveChangesAsync();

        }

        public async Task<List<DurationDTO>> GetHoursByEmployee(DateTime startDate, DateTime endDate, int employeeId)
        {
            var employee = await _context.Employees.AsNoTracking().FirstOrDefaultAsync(x => x.Id == employeeId);



            var schedule = await _context.Schedules
                  .Where(x => x.WorkingDate >= startDate 
                    && x.WorkingDate <= endDate 
                    && x.Employees.Contains(employee)
                    && x.IsDone == true)
           .Include(x => x.Project)
           .OrderBy(x => x.WorkingDate)        
           .ToListAsync();

        
            var durationList = new List<DurationDTO>();
            foreach (var item in schedule)
            {
                var duration = new DurationDTO();
                duration.WorkingDate = item.WorkingDate;
                duration.ProjectId = item.Project.Id;
                var time = (TimeSpan)(item.EndTime - item.StartTime);
                duration.Duration = time.TotalHours.ToString();
                durationList.Add(duration);
            }

            return durationList;
        }

        public async Task<EmployeeDTO> GetEmployeeByUserId(int userId)
        {
            var employee = await _context.Employees
                .Include(x => x.User)
                .Include(x => x.Address)
                .FirstOrDefaultAsync(x => x.UserId == userId);

            EmployeeDTO employeeDTO = ObjectMapper.Mapper.Map<EmployeeDTO>(employee);

            return employeeDTO;
        }

        public async Task SuspendEmployee(int id)
        {
            var employeeToSuspend = await _context.Employees.FirstOrDefaultAsync(e => e.Id == id);
          
            var futureJobs = await _context.Schedules
           .Where(x => x.Employees.Contains(employeeToSuspend) && x.WorkingDate > DateTime.Now)
           .Include(x => x.Employees)
           .ToListAsync();

         
                if (employeeToSuspend.IsActive)
                {
                    employeeToSuspend.IsActive = false;

                     foreach (var item in futureJobs)
                      {
                             item.Employees.Remove(employeeToSuspend);
                       }

                 }
                else
                {
                    employeeToSuspend.IsActive = true;
                }
     

            await _context.SaveChangesAsync();
        }
    }
}

