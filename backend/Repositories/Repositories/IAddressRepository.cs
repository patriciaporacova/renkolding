﻿using Domain.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Repositories.Repositories
{
    public interface IAddressRepository
    {
        Task<AddressDTO> AddAddress(AddressDTO addressDTO);
        Task<AddressDTO> GetAddress(int id);
    }
}
