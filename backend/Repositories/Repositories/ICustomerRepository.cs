﻿
using Domain.Models;
using System;
using System.Collections.Generic;

using System.Linq;
using System.Threading.Tasks;

namespace Repositories
{
    public interface ICustomerRepository
    {
        Task<IList<CustomerDTO>> GetAllCustomers();
        Task<IList<CustomerDTO>> GetAllActiveCustomers();
        Task<CustomerDTO> AddCustomer(CustomerDTO customer);
        Task UpdateCustomer(CustomerDTO customerDTO);
        Task<CustomerDTO> GetCustomer(int id);
        Task<bool> IsCvrAvailable(string cvr);
        Task<bool> IsEmailAvailable(string email);
        Task DeleteCustomer(int id);
        Task AssignNewAddress(int customerId, int addresId);
        Task SuspendCustomer(int id);
    }
}
