﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Domain.Models;
using EfDataAccess.Entities;

namespace Repositories
{
    public interface IEmployeeRepository
    {
        Task<IList<EmployeeDTO>> GetAllEmployees();
        Task<IList<EmployeeDTO>> GetAllActiveEmployees();
        Task<EmployeeDTO> CreateEmployee(EmployeeDTO employeeDTO, UserDTO userDTO);
        Task UpdateEmployee(EmployeeDTO employeeDTO);
        Task<EmployeeDTO> GetEmployee(int id);
        Task DeleteEmployee(int id);
        Task AssignNewAddress(int employeeId, int addresId);
        Task<List<DurationDTO>> GetHoursByEmployee(DateTime startDate, DateTime endDate, int employeeId);
        Task<EmployeeDTO> GetEmployeeByUserId(int userId);
        Task SuspendEmployee(int id);
    }
}
