﻿using Domain.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Repositories.Repositories
{
    public interface IMaterialRepository
    {
        Task<MaterialDTO> AddMaterial(MaterialDTO materialDTO);
        Task<IList<MaterialDTO>> GetAllMaterials();
        Task<MaterialDTO> GetMaterial(int id);
        Task<IList<MaterialDTO>> GetMaterialsByJob(int jobId);
    }
}
