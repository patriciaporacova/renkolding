﻿
using Domain.Models;
using EfDataAccess.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Repositories
{
    public interface IProjectRepository
    {
        Task<ProjectDTO> AddProject(ProjectDTO project);
        Task DeleteProject(int id);
        Task UpdateProject(ProjectDTO projectDTO);
        Task<IList<ProjectDTO>> GetProjectsByCustomerId(int id);
        Task<ProjectDTO> GetProject(int id);
        Task AssignNewAddress(int projectId, int addresId);
        Task SuspendProject(int id);
    }
}
