﻿using Domain.Models;
using EfDataAccess.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Repositories
{
    public interface IScheduleRepository
    {

        Task<IList<ScheduleDTO>> GetSchedule(DateTime startDate, DateTime endDate);
        Task<ScheduleDTO> AddJob(ScheduleDTO scheduleDTO);
        Task<ScheduleDTO> GetJob(int id);
        Task<IList<ScheduleDTO>> GetCurrentMonthSchedule();
        Task AssignEmployee(int employeeId, int jobId);
        Task<IList<ScheduleDTO>> AddJobRepeat(ScheduleDTO scheduleDTO, int repeat, DateTime endRepeatDate);
        Task<IList<ScheduleDTO>>GetByEmployee(DateTime startDate, DateTime endDate, int employeeId);
        Task UpdateJOb(ScheduleDTO jobDTO);
        Task DeleteJob(int id);
        Task AddMaterialToJob(int jobId, int materialId);
        Task SetJobAsDone(int jobId);
    }
}
