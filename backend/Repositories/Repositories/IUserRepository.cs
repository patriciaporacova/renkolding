using System.Collections.Generic;
using System.Threading.Tasks;
using Domain.Models;

namespace Repositories
{
    public interface IUserRepository
    {
        Task<bool> IsUserValidatedAsync(string userName, string password);
        Task<bool> IsUserAdmin(string username);
        Task<bool> IsValidUserCredentialsAsync(string userName, string password);
        Task<string> GetUserRole(string userName);
        Task<bool> IsEmailAvailable(string email);
        Task<int> GetUserId(string userName);
    }
}