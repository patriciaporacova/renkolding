﻿using Domain.Models;
using EfDataAccess;
using EfDataAccess.Entities;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Repositories.Repositories
{
    public class MaterialRepository : IMaterialRepository
    {
        protected readonly RenKoldingDB_Context _context;


        public MaterialRepository(RenKoldingDB_Context context)
        {
            _context = context;
        }



        public async Task<MaterialDTO> AddMaterial(MaterialDTO materialDTO)
        {
            var dest = ObjectMapper.Mapper.Map<Material>(materialDTO);
            var result = await _context.Materials.AddAsync(dest);
            await _context.SaveChangesAsync();
            var newMaterialDTO = ObjectMapper.Mapper.Map<MaterialDTO>(result.Entity);
            return newMaterialDTO;
        }

        public async Task<IList<MaterialDTO>> GetAllMaterials()
        {
            var materials = await _context.Materials.ToListAsync();
            List<MaterialDTO> materialDTOs = ObjectMapper.Mapper.Map<List<MaterialDTO>>(materials);

            return materialDTOs;
        }

        public async Task<MaterialDTO> GetMaterial(int id)
        {
            var material = await _context.Materials.FirstOrDefaultAsync(x => x.Id == id);

            var materialDTO = ObjectMapper.Mapper.Map<MaterialDTO>(material);

            return materialDTO;
        }

        public async Task<IList<MaterialDTO>> GetMaterialsByJob(int jobId)
        {
            var job = await _context.Schedules
                .Include(x => x.Materials)
                .FirstOrDefaultAsync(x => x.Id == jobId);
                                   
                                    
            List<MaterialDTO> materialDTOs = ObjectMapper.Mapper.Map<List<MaterialDTO>>(job.Materials);

            return materialDTOs;
        }
    }
}
