﻿using EfDataAccess;
using Domain.Models;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using EfDataAccess.Entities;
using AutoMapper;

namespace Repositories
{
    public class ProjectRepository : IProjectRepository
    {
        protected readonly RenKoldingDB_Context _context;


        public ProjectRepository(RenKoldingDB_Context context)
        {
            _context = context;

        }

        public async Task DeleteProject(int id)
        {
            var projectTodelete = await _context.Projects.FirstOrDefaultAsync(e => e.Id == id);
            var isProjectInSchedule = await _context.Schedules
                 .Where(x => x.Project.Id == projectTodelete.Id)
                 .ToListAsync();
                        
            var futureJobs = await _context.Schedules
                .Where(x => x.Project.Id == projectTodelete.Id && x.WorkingDate > DateTime.Now)
                .ToListAsync();

            if (isProjectInSchedule.Count == 0)
            {
                _context.Projects.Remove(projectTodelete);
            }
            else
            {
                projectTodelete.IsActive = false;
                _context.Schedules.RemoveRange(futureJobs);
            }

            await _context.SaveChangesAsync();
        }

        public async Task UpdateProject(ProjectDTO projectDTO)
        {
            var employee = ObjectMapper.Mapper.Map<Project>(projectDTO);
            var result = await _context.Projects
                .FirstOrDefaultAsync(e => e.Id == projectDTO.Id);

            if (result != null)
            {
                _context.Entry(result).CurrentValues.SetValues(projectDTO);
            }


            await _context.SaveChangesAsync();

        }

        public async Task<ProjectDTO> AddProject(ProjectDTO project)
        {
            var dest = ObjectMapper.Mapper.Map<Project>(project);
            var result = await _context.Projects.AddAsync(dest);
            await _context.SaveChangesAsync();
            var projectDTO = ObjectMapper.Mapper.Map<ProjectDTO>(result.Entity);
            return projectDTO;
        }

        public async Task<IList<ProjectDTO>> GetProjectsByCustomerId(int id)
        {
            var projects = await _context.Projects
                .Where(x => x.CustomerId == id)
                .Include(x => x.Address)
                .ToListAsync();

            List<ProjectDTO> custDto = new List<ProjectDTO>();
            foreach (var item in projects)
            {
                custDto.Add(ObjectMapper.Mapper.Map<ProjectDTO>(item));
            }

            return custDto;
        }

        public async Task<ProjectDTO> GetProject(int id)
        {
            var project = await _context.Projects
            .Include(x => x.Address)
            .FirstOrDefaultAsync(x => x.Id == id);

            var projectDTO = ObjectMapper.Mapper.Map<ProjectDTO>(project);
            return projectDTO;
        }

        public async Task AssignNewAddress(int projectId, int addresId)
        {
            var project = await _context.Projects.FirstOrDefaultAsync(e => e.Id == projectId);

            project.AddressId = addresId;
            await _context.SaveChangesAsync();
        }

        public async Task SuspendProject(int id)
        {
            var projectToSuspend = await _context.Projects.FirstOrDefaultAsync(e => e.Id == id);

            var futureJobs = await _context.Schedules
           .Where(x => x.Project.Id == projectToSuspend.Id && x.WorkingDate > DateTime.Now)
           .ToListAsync();


            if (projectToSuspend.IsActive)
            {
                projectToSuspend.IsActive = false;
                _context.Schedules.RemoveRange(futureJobs);

            }
            else
            {
                projectToSuspend.IsActive = true;
            }


            await _context.SaveChangesAsync();
        }
    }
}
