﻿using Domain.Models;
using EfDataAccess;
using EfDataAccess.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repositories
{
    public class ScheduleRepository : IScheduleRepository
    {
        private DateTime defaulEnDate = DateTime.Now.AddYears(2);
        protected readonly RenKoldingDB_Context _context;

        public ScheduleRepository(RenKoldingDB_Context context)
        {
            _context = context;
        }

        public async Task<ScheduleDTO> AddJob(ScheduleDTO scheduleDTO)
        {
            Schedule schedule = ObjectMapper.Mapper.Map<Schedule>(scheduleDTO);

            var employees = new List<Employee>();

            if (scheduleDTO.EmployeeIds != null)
            {
                foreach (var item in scheduleDTO.EmployeeIds)
                {
                    var emp = await _context.Employees.FirstOrDefaultAsync(x => x.Id == item);
                    employees.Add(emp);
                }
                schedule.Employees = employees;
            }
           
            var result = await _context.Schedules.AddAsync(schedule);
            await _context.SaveChangesAsync();
            return ObjectMapper.Mapper.Map<ScheduleDTO>(result.Entity);

        }

        public async Task<IList<ScheduleDTO>> AddJobRepeat(ScheduleDTO scheduleDTO, int repeat, DateTime endRepeatDate)
        {
            var newJobs = new List<ScheduleDTO>();
            if (endRepeatDate > defaulEnDate)
            {
                endRepeatDate = defaulEnDate;
            }
            if (repeat == 0)
            {
                var job = await AddJob(scheduleDTO);
                newJobs.Add(job);
            }
            else
            {
                do
                {
                    if (repeat == 30)
                    {
                        var job = await AddJob(scheduleDTO);
                        newJobs.Add(job);
                        var nextMonth = scheduleDTO.WorkingDate.AddMonths(1);
                        scheduleDTO.WorkingDate = nextMonth;

                    }
                    else
                    {
                        var job = await AddJob(scheduleDTO);
                        newJobs.Add(job);
                        var newDate = scheduleDTO.WorkingDate.AddDays(repeat);
                        scheduleDTO.WorkingDate = newDate;

                    }

                }

                while (scheduleDTO.WorkingDate <= endRepeatDate.AddDays(1));
            }
            await _context.SaveChangesAsync();


            return newJobs;
        }

        public async Task AssignEmployee(int employeeId, int jobId)
        {
            var job = await _context.Schedules
                .Include(x => x.Employees)
                .FirstOrDefaultAsync(x => x.Id == jobId);
            var employee = await _context.Employees.FirstOrDefaultAsync(x => x.Id == employeeId);

            job.Employees.Add(employee);

            await _context.SaveChangesAsync();
        }

        public async Task<IList<ScheduleDTO>> GetCurrentMonthSchedule()
        {
            var now = DateTime.Now;
            var first = new DateTime(now.Year, now.Month, 1);
            var last = first.AddMonths(1).AddDays(-1);



            return await GetSchedule(first, last);
        }

        public async Task<ScheduleDTO> GetJob(int id)
        {
            var job = await _context.Schedules
                 .Include(x => x.Project)
                .Include(x => x.Customer)
                .Include(x => x.Employees)

                .FirstOrDefaultAsync(x => x.Id == id);

            ScheduleDTO scheduleDTO = ObjectMapper.Mapper.Map<ScheduleDTO>(job);

            return scheduleDTO;
        }



        public async Task<IList<ScheduleDTO>> GetSchedule(DateTime startDate, DateTime endDate)
        {
            var schedule = await _context.Schedules
              .Where(x => x.WorkingDate >= startDate && x.WorkingDate <= endDate)
              .Include(x => x.Project)
              .Include(x => x.Customer)
              .Include(x => x.Employees)
              .ToListAsync();

            List<ScheduleDTO> scheduleDTO = ObjectMapper.Mapper.Map<List<ScheduleDTO>>(schedule);

            return scheduleDTO;
        }

        public async Task<IList<ScheduleDTO>> GetByEmployee(DateTime startDate, DateTime endDate, int employeeId)
        {
            var employee = await _context.Employees.AsNoTracking().FirstOrDefaultAsync(x => x.Id == employeeId);

            var schedule = await _context.Schedules
           .Where(x => x.WorkingDate >= startDate && x.WorkingDate <= endDate && x.Employees.Contains(employee))
           .Include(x => x.Project)
           .Include(x => x.Customer)
           .OrderBy(x => x.WorkingDate)
           .ToListAsync();

            List<ScheduleDTO> scheduleDTO = ObjectMapper.Mapper.Map<List<ScheduleDTO>>(schedule);

            return scheduleDTO;
        }



        public async Task UpdateJOb(ScheduleDTO jobDTO)
        {

            var existingJob = await _context.Schedules.FirstOrDefaultAsync(x => x.Id == jobDTO.Id);


            var newEmployeeList = new List<Employee>();
            foreach (var item in jobDTO.EmployeeIds)
            {
                var employee = await _context.Employees.Include(x => x.Address).FirstOrDefaultAsync(x => x.Id == item);
                newEmployeeList.Add(employee);
            }
            var customer = await _context.Customers.Include(x => x.Address).FirstOrDefaultAsync(x => x.Id == jobDTO.CustomerId);
            var project = await _context.Projects.Include(x => x.Address).FirstOrDefaultAsync(x => x.Id == jobDTO.ProjectId);


            existingJob.WorkingDate = jobDTO.WorkingDate;
            existingJob.StartTime = TimeSpan.Parse(jobDTO.StartTime);
            existingJob.EndTime = TimeSpan.Parse(jobDTO.EndTime);
            existingJob.TodayTasks = jobDTO.TodayTasks;
            existingJob.Employees = newEmployeeList;
            existingJob.Customer = customer;
            existingJob.Project = project;
            existingJob.IsDone = jobDTO.IsDone;

            await _context.SaveChangesAsync();

        }

        public async Task DeleteJob(int id)
        {
            var jobToDelete = await _context.Schedules.FirstOrDefaultAsync(a => a.Id == id);
            _context.Schedules.Remove(jobToDelete);
            await _context.SaveChangesAsync();
        }

        public async Task AddMaterialToJob(int jobId, int materialId)
        {
            var job = await _context.Schedules
                            .Include(x => x.Materials)
                            .FirstOrDefaultAsync(x => x.Id == jobId);

            var material = await _context.Materials.FirstOrDefaultAsync(x => x.Id == materialId);

            job.Materials.Add(material);

            await _context.SaveChangesAsync();
        }

        public async Task SetJobAsDone(int jobId)
        {
            var job = await _context.Schedules.FirstOrDefaultAsync(x => x.Id == jobId);

            job.IsDone = true;

            await _context.SaveChangesAsync();
        }
    }
}
