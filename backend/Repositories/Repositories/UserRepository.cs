using System.Threading.Tasks;
using Domain.Models;
using EfDataAccess;
using Microsoft.EntityFrameworkCore;
using Repositories.Password;

namespace Repositories
{
    public class UserRepository :  IUserRepository
    {
        protected readonly RenKoldingDB_Context _context;
        private readonly IPasswordHasher _passwordHasher;

        public UserRepository(RenKoldingDB_Context context, IPasswordHasher passwordHasher)
        {
            _context = context;
            _passwordHasher = passwordHasher;
        }

        public async Task<bool> IsUserAdmin(string username)
        {
            var user = await _context.Users.FirstOrDefaultAsync(x => x.Username == username);

            return user != null && user.IsAdmin.Equals(true);
        }

        public async Task<bool> IsUserValidatedAsync(string username, string password)
        {
            var user = await _context.Users.FirstOrDefaultAsync(x => x.Username == username);

            var check = _passwordHasher.Check(user.Password, password);
         
            return user != null && check;
      

        }

        public async Task<bool> IsValidUserCredentialsAsync(string userName, string password)
        {
            
            return await IsUserValidatedAsync(userName, password);
        }

        public async Task<string> GetUserRole(string userName)
        {

            return await IsUserAdmin(userName) ? UserRoles.Admin : UserRoles.BasicUser;
        }

        public async Task<bool> IsEmailAvailable(string email)
        {
            var isAv = await _context.Users.AllAsync(u =>
                    !u.Username.Equals(email)//ignores case by default
                );

            return isAv;
        }

        public async Task<int> GetUserId(string userName)
        {
            var user = await _context.Users.FirstOrDefaultAsync(x => x.Username == userName);
            return user.Id;
        }
    }
}