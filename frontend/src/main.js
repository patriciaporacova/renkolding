import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from "./store/store";
import ColourPicker  from 'vue-colour-picker'
Vue.component('colour-picker', ColourPicker)

import 'vue-awesome/icons/regular/clipboard'
import 'vue-awesome/icons/regular/trash-alt'
import 'vue-awesome/icons/regular/plus-square'
import 'vue-awesome/icons/times'
import 'vue-awesome/icons/cog'
import 'vue-awesome/icons/caret-down'
import 'vue-awesome/icons/sign-out-alt'
import 'vue-awesome/icons/location-arrow'
import 'vue-awesome/icons/regular/envelope'
import 'vue-awesome/icons/mobile-alt'
import 'vue-awesome/icons/regular/building'
import 'vue-awesome/icons/regular/user'
import 'vue-awesome/icons/regular/clock'
import 'vue-awesome/icons/dollar-sign'
import 'vue-awesome/icons/regular/edit'
import 'vue-awesome/icons/regular/calendar-alt'
import Icon from 'vue-awesome/components/Icon'
Vue.component('v-icon', Icon)

import 'vue-loaders/dist/vue-loaders.css';
import VueLoaders from 'vue-loaders';
Vue.component('loader', VueLoaders.component);

import Avatar from 'vue-avatar-component'
Vue.component('avatar', Avatar)

import ModalFullScreenVue from 'modal-fullscreen-vue'
Vue.use('full-screen-modal',ModalFullScreenVue)

import VueSweetalert2 from 'vue-sweetalert2';
import 'sweetalert2/dist/sweetalert2.min.css';

Vue.use(VueSweetalert2);


// import { makeServer } from "./mock-server";
//
// if (process.env.NODE_ENV === "development") {
//     makeServer();
// }


import {VueSpinners} from '@saeris/vue-spinners';
Vue.use(VueSpinners);



Vue.config.productionTip = false
const app = new Vue({
    router,
    store,
    render: h => h(App)
}).$mount('#app')

store.$app = app;