import {createServer} from "miragejs";

export function makeServer() {
    createServer({
        routes() {
            this.namespace = "api"

            this.get("/employees", () => {
                return {
                    employees: [
                        {
                            "userId": 2,
                            "firstName": "Harpreet",
                            "lastName": "Kaura",
                            "phone": "569162478",
                            "picture": null,
                            "email": "hpr@gg.com",
                            "isActive": true,
                            "address": {
                                "id": 48,
                                "addressLine1": "Byvej 78",
                                "addressLine2": "2.TV",
                                "city": "Kolding",
                                "zip": "6000",
                                "country": "Danmark"
                            },
                            "user": null
                        },
                        {
                            "userId": 3,
                            "firstName": "Marina",
                            "lastName": "Estevera",
                            "phone": "543432",
                            "picture": null,
                            "email": "marina@gg.com",
                            "isActive": true,
                            "address": {
                                "id": 48,
                                "addressLine1": "Byvej 78",
                                "addressLine2": "2.TV",
                                "city": "Kolding",
                                "zip": "6000",
                                "country": "Danmark"
                            },
                            "user": null
                        }
                    ],

                }
            })

            // Responding to a POST request
            this.post("/employees", (schema, request) => {
                let attrs = JSON.parse(request.requestBody)
                attrs.id = Math.floor(Math.random() * 100)
                return {employee: attrs}
            })

            this.get("/customers", () => {
                return {
                    customers: [
                        {
                            "id": 3,
                            "name": "GYU Media Group, APS",
                            "cvr": "78906493",
                            "phone": "67436533",
                            "email": "gyu@media.dk",
                            "contactperson": "Maria Nielsen",
                            "isBusiness": true,
                            "isActive": true,
                            "address": {
                                "id": 29,
                                "addressLine1": "Fynsvej 34",
                                "addressLine2": null,
                                "city": "Kolding",
                                "zip": "6000",
                                "country": "Danmark"
                            },
                            "projects": [
                                {
                                    "projectId": 1,
                                    "customerId": 3,
                                    "name": "Strandvej",
                                    "isHourlyPaid": true,
                                    "hourlyRate": 420,
                                    "pricePerPiece": null,
                                    "projectDescription": "Short descrition",
                                    "taskDescription": "Tasks:.....",
                                    "isActive": true,
                                    "address": {
                                        "id": 41,
                                        "addressLine1": "Stranvej 34",
                                        "addressLine2": null,
                                        "city": "Kolding",
                                        "zip": "6000",
                                        "country": "Danmark"
                                    }
                                },
                                {
                                    "projectId": 2,
                                    "customerId": 3,
                                    "name": "StranvejHUS",
                                    "isHourlyPaid": true,
                                    "hourlyRate": 420,
                                    "pricePerPiece": null,
                                    "projectDescription": "Description",
                                    "taskDescription": "Tasks:....",
                                    "isActive": true,
                                    "address": {
                                        "id": 42,
                                        "addressLine1": "Stranvej 55",
                                        "addressLine2": null,
                                        "city": "Kolding",
                                        "zip": "6000",
                                        "country": "Danmark"
                                    }
                                },
                                {
                                    "projectId": 4,
                                    "customerId": 3,
                                    "name": "Reception",
                                    "isHourlyPaid": false,
                                    "hourlyRate": null,
                                    "pricePerPiece": 1200,
                                    "projectDescription": "Description",
                                    "taskDescription": "Tasks:...",
                                    "isActive": false,
                                    "address": {
                                        "id": 45,
                                        "addressLine1": "address 45",
                                        "addressLine2": "string",
                                        "city": "Kolding",
                                        "zip": "6000",
                                        "country": "Danmark"
                                    }
                                },
                                {
                                    "projectId": 5,
                                    "customerId": 3,
                                    "name": "Butik",
                                    "isHourlyPaid": false,
                                    "hourlyRate": null,
                                    "pricePerPiece": 1500,
                                    "projectDescription": "string",
                                    "taskDescription": "string",
                                    "isActive": true,
                                    "address": {
                                        "id": 46,
                                        "addressLine1": "Bjregbyvej 45",
                                        "addressLine2": null,
                                        "city": "Kolding",
                                        "zip": "6000",
                                        "country": "Danmark"
                                    }
                                }
                            ]
                        },
                        {
                            "id": 2,
                            "name": "Mango Dbs Ali",
                            "cvr": "78906493",
                            "phone": "67436533",
                            "email": "gyu@media.dk",
                            "contactperson": "Maria Nielsen",
                            "isBusiness": true,
                            "isActive": true,
                            "address": {
                                "id": 29,
                                "addressLine1": "Fynsvej 34",
                                "addressLine2": null,
                                "city": "Kolding",
                                "zip": "6000",
                                "country": "Danmark"
                            },
                            "projects": [
                                {
                                    "projectId": 11,
                                    "customerId": 2,
                                    "name": "Strandvej",
                                    "isHourlyPaid": true,
                                    "hourlyRate": 420,
                                    "pricePerPiece": null,
                                    "projectDescription": "Short descrition",
                                    "taskDescription": "Tasks:.....",
                                    "isActive": true,
                                    "address": {
                                        "id": 41,
                                        "addressLine1": "Stranvej 34",
                                        "addressLine2": null,
                                        "city": "Kolding",
                                        "zip": "6000",
                                        "country": "Danmark"
                                    }
                                },
                                {
                                    "projectId": 12,
                                    "customerId": 2,
                                    "name": "StranvejHUS",
                                    "isHourlyPaid": true,
                                    "hourlyRate": 420,
                                    "pricePerPiece": null,
                                    "projectDescription": "Description",
                                    "taskDescription": "Tasks:....",
                                    "isActive": true,
                                    "address": {
                                        "id": 42,
                                        "addressLine1": "Stranvej 55",
                                        "addressLine2": null,
                                        "city": "Kolding",
                                        "zip": "6000",
                                        "country": "Danmark"
                                    }
                                },
                                {
                                    "projectId": 14,
                                    "customerId": 2,
                                    "name": "Reception",
                                    "isHourlyPaid": false,
                                    "hourlyRate": null,
                                    "pricePerPiece": 1200,
                                    "projectDescription": "Description",
                                    "taskDescription": "Tasks:...",
                                    "isActive": false,
                                    "address": {
                                        "id": 45,
                                        "addressLine1": "address 45",
                                        "addressLine2": "string",
                                        "city": "Kolding",
                                        "zip": "6000",
                                        "country": "Danmark"
                                    }
                                },
                                {
                                    "projectId": 15,
                                    "customerId": 2,
                                    "name": "Butik",
                                    "isHourlyPaid": false,
                                    "hourlyRate": null,
                                    "pricePerPiece": 1500,
                                    "projectDescription": "string",
                                    "taskDescription": "string",
                                    "isActive": true,
                                    "address": {
                                        "id": 46,
                                        "addressLine1": "Bjregbyvej 45",
                                        "addressLine2": null,
                                        "city": "Kolding",
                                        "zip": "6000",
                                        "country": "Danmark"
                                    }
                                }
                            ]
                        }]

                }
            })

            this.post("/customers", (schema, request) => {
                let attrs = JSON.parse(request.requestBody)
                attrs.id = Math.floor(Math.random() * 100)
                return {customer: attrs}
            })

        },
    })
}
