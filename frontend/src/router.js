import Vue from 'vue'
import Router from 'vue-router'
import store from "./store/store";

import Admin from "./views/Admin.vue";
import Dashboard from "./views/Dashboard.vue";
import Calendar from "./views/Calendar.vue";
import UserCalendar from "./views/UserCalendar.vue";
import User from "./views/Employee.vue";
import Profile from "./views/Profile.vue";
import Login from "./views/Login.vue";

Vue.use(Router);

const checkAuth = (to, from, next) => {

    //Admin for admin BasicUser for user ..can be modified
    if (store.getters.role === 'Admin') {
        return next({name: 'dashboard'});
    } else if (store.getters.role === 'BasicUser') {
        return next({name: 'user-calendar'});
    } else {
        return next();
    }

};

const checkAdminRole = (to, from, next) => {
    if (store.getters.role === 'Admin' && to.name === 'dashboard') {
        return next();
    } else next({name: 'login'});
};

const checkUserRole = (to, from, next) => {
    if (store.getters.role === 'BasicUser' && to.name === 'user-calendar') {
        return next();
    } else next({name: 'login'});
};

const routes = [
    {path: '/login', name: 'login', component: Login, beforeEnter: checkAuth},
    {
        path: '/user',
        name: 'user',
        component: User,
        children: [
            {
                path: "/user/profile",
                name: "profile",
                component: Profile
            },
            {
                path: "/user/calendar",
                name: "user-calendar",
                component: UserCalendar
            },
        ],
        beforeEnter: checkUserRole
    },
    {
        path: '/admin',
        name: 'admin',
        component: Admin,
        children: [
            {
                path: "/admin/dashboard",
                name: "dashboard",
                component: Dashboard
            },
            {
                path: "/admin/calendar",
                name: "admin-calendar",
                component: Calendar
            },
        ],
        beforeEnter: checkAdminRole
    },
    {path: '/*', redirect: '/login'},
]


const router = new Router({mode: 'history', routes, linkExactActiveClass: "active"})

router.beforeEach((to, from, next) => {

    if (to.name === 'login') {
        return next();
    }

    const role = localStorage.getItem('user-role');

    if (!role) {
        return next({name: 'login'});
    } else {
        store.commit('set_role', role);
    }

    return next();
});


export default router;
