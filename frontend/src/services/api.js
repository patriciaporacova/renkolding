import axios from 'axios';
import store from "../store/store.js";
import router from "../router";

export default () => {
    const api = axios.create({
        baseURL: 'https://localhost:5001/',
        //timeout : 1000 //1s
        headers: {"Content-Type": "application/json",}
    });

    // request setting
    api.interceptors.request.use((config) => {
        config.headers.Authorization = `Bearer ${localStorage.getItem('user-token')}`;
        config.headers.Accept = '*/*';
        return config;
    });


    // interceptor to catch errors
    const errorInterceptor = error => {
        // all the error responses
        switch (error.response.status) {
            case 400:
                alert('Wrong input')
                console.error(error.response.status, error.message);
                break;
            case 401: // authentication error, logout the user
                localStorage.removeItem('user-token');
                localStorage.removeItem('user-role');
                localStorage.removeItem('user-id');
                router.push('/login');
                break;
            default:
                console.error(error.response.status, error.message);

        }
        return Promise.reject(error);
    }

// Interceptor for responses
    const responseInterceptor = response => {
        switch (response.status) {
            case 200:
                // yay!
                break;
            // any other cases
            default:
            // default case
        }

        return response;
    }

    api.interceptors.response.use(responseInterceptor, errorInterceptor);


    return api;
};
