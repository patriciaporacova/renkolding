const DetailViewEnums = Object.freeze({"customer":"customer", "employee":"employee", "project":"project","job":"job", 'user_job':"user_job"})

export default {
    DetailViewEnums
}