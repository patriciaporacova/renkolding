import Api from './api.js'

export default {
    // add(x) {
    //     return Api().post('/endpoint', [x]);
    // },
    // get() {
    //     return Api().get('/endpoint');
    // },
    // update(id, type, data) {
    //     return Api().put(`/endpoint/${id}/${type}`, data);
    // },
    // deletePeople(x) {
    //     return Api().delete('/endpoint', {data: x});
    // },

    authUser(credentials) {
        return Api().post('/api/Account/login', credentials);
    },

    getCustomers(){
        return Api().get('/api/Customer');
    },

    addCustomer(customer) {
        return Api().post('/api/Customer', customer);
    },

    getEmployees(){
        return Api().get('/api/Employee');
    },

    addEmployee(employee) {
        return Api().post('/api/Employee', employee);
    },

};