import Api from "@/services/api";

const state = {
    addressLoading: false,
    addressError: false,
    addressSuccess: false,
};

const mutations = {
    'SET_ADDRESS_LOADING'(state) {
        state.employeesLoading = true;
    },
    'SET_ADDRESS_LOADED'(state) {
        state.employeesLoading = false;
    },
    'SET_ADDRESS_ERROR'(state) {
        state.employeeError = true;
    },
    'SET_ADDRESS_ERROR_RESOLVE'(state) {
        state.employeeError = false;
    },
    'SET_ADDRESS_SUCCESS'(state) {
        state.employeeSuccess = true;
    },
    'RESET_ADDRESS_SUCCESS'(state) {
        state.employeeSuccess = false;
    },
}

const actions = {
    async addAddress({commit}, payload) {
        commit('RESET_ADDRESS_SUCCESS');
        commit('SET_ADDRESS_LOADING');
        let response = await Api().post('/api/Address', payload).catch(err => {
            commit('SET_ADDRESS_LOADED');
            commit('SET_ADDRESS_ERROR');
        })
        commit('SET_ADDRESS_LOADED');
        commit('SET_ADDRESS_SUCCESS');
        return response.data
    },
}

const getters = {
    addressLoading: state => {
        return state.addressLoading;
    },
    addressError: state => {
        return state.addressError;
    },
    addressSuccess: state => {
        return state.addressSuccess;
    },
}


export default {
    state, mutations, actions, getters
}