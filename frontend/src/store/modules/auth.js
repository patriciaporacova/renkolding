import Api from '../../services/api.js'

const state = {
    token: localStorage.getItem('user-token') || '',
    role: localStorage.getItem('user-role') || '',
    userId: localStorage.getItem('user-id') || '',
    status: '',
};

const mutations = {
    auth_request: (state) => {
        state.status = 'loading'
    },
    set_role: (state, role) => {
        state.role = role;
    },
    set_id: (state, id) => {
        state.userId = id;
    },
    auth_success: (state, token) => {
        state.status = 'success'
        state.token = token
    },
    auth_error: (state) => {
        state.status = 'error'
    },
};

const actions = {

    // authUser: ({commit, dispatch}, user) => {
    authUser({commit, dispatch}, user) {
        commit('auth_request')
        Api().post('api/Account/login', user).then(
            response => {
                const token = response.data.accessToken
                const role = response.data.role
                const id = response.data.id
                localStorage.setItem('user-token', token) // store the token in localstorage
                localStorage.setItem('user-role', role) // store the role in localstorage
                localStorage.setItem('user-id', id) // store the id in localstorage
                commit('auth_success', token)
                commit('set_role', role)
                commit('set_id', id)
            }
        ).catch(err => {
            commit('auth_error', err)
            localStorage.removeItem('user-token') // if the request fails, remove any possible user token if possible
            localStorage.removeItem('user-role')
            localStorage.removeItem('user-id')

        })
    },
    logOut: ({commit}) => {
        return new Promise((resolve, reject) => {
            localStorage.removeItem('user-token');
            localStorage.removeItem('user-role');
            localStorage.removeItem('user-id');
            state.role = -1;
            state.userId = -1;
            resolve()
        })
    },
};

const getters = {
    token: state => !!state.token,
    role: state => state.role,
    userId: state => state.userId,
    authStatus: state => state.status,
};

export default {
    state, mutations, actions, getters
}
