import axios from "axios";
import Api from "@/services/api";

const state = {
    customers: [],
    customersLoading: false,
    customerSuccess: false,
    customerError: false,
    showModal: false,
};

const mutations = {
    'CUSTOMERS_LOADING'(state) {
        state.customersLoading = true;
    },
    'CUSTOMERS_LOADED'(state) {
        state.customersLoading = false;
    },
    'OPEN_MODAL'(state) {
        state.showModal = true;
    },
    'CLOSE_MODAL'(state) {
        state.showModal = false;
    },
    'SET_CUSTOMER_ERROR'(state) {
        state.customerError = true;
    },
    'SET_CUSTOMER_ERROR_RESOLVE'(state) {
        state.customerError = false;
    },
    'SET_CUSTOMER_SUCCESS'(state) {
        state.customerSuccess = true;
    },
    'RESET_CUSTOMER_SUCCESS'(state) {
        state.customerSuccess = false;
    },

    'SET_CUSTOMERS'(state, customers) {
        state.customers = customers.map(customer => ({
            id: customer.id,
            name: customer.name,
            uiColorCode: customer.uiColorCode,
            cvr: customer.cvr,
            phone: customer.phone,
            email: customer.email,
            contactperson: customer.contactperson,
            isBusiness: customer.isBusiness,
            isActive: customer.isActive,
            address: {
                addressLine1: customer.address.addressLine1,
                addressLine2: customer.address.addressLine2,
                city: customer.address.city,
                zip: customer.address.zip,
                country: customer.address.country
            },
            projects: customer.projects.map(project => ({
                id: project.id,
                customerId: project.customerId,
                name: project.name,
                isHourlyPaid: project.isHourlyPaid,
                hourlyRate: project.hourlyRate,
                uiColorCode: project.uiColorCode,
                pricePerPiece: project.pricePerPiece,
                projectDescription: project.projectDescription,
                taskDescription: project.taskDescription,
                isActive: project.isActive,
                address: {
                    addressLine1: project.address.addressLine1,
                    addressLine2: project.address.addressLine2,
                    city: project.address.city,
                    zip: project.address.zip,
                    country: project.address.country
                }
            }))
        }));
    },
    'SET_CUSTOMERS_PROJECTS'(state, project) {
        let customersProjects = state.customers.find(customer => customer.id === project.customerId).projects
        state.customers.find(customer => customer.id === project.customerId).projects = [...customersProjects, project]
    },


}

const actions = {
    closeCustomerShowingModal: ({commit}) => {
        commit('CLOSE_MODAL');
    },
    getCustomers: ({commit}) => {
        commit('CUSTOMERS_LOADING');
        Api().get('api/Customer').then(response => {
                commit('CUSTOMERS_LOADED');
                commit('SET_CUSTOMERS', response.data)
            }
        )
    },
    addCustomer: ({commit, dispatch}, payload) => {
        Api().post('api/Customer', payload).then(response => {
                let customers = [...state.customers, response.data]
                commit('SET_CUSTOMERS', customers)
            }
        )
    },
    addProject: ({commit, dispatch}, {payload, id}) => {
        Api().post(`api/Project/${id}`, payload).then(response => {
                commit('SET_CUSTOMERS_PROJECTS', response.data)
            }
        )
    },
    editProject: ({commit, dispatch}, payload) => {
        commit('RESET_CUSTOMER_SUCCESS');
        commit('CUSTOMERS_LOADING');
        Api().put(`/api/Project`, payload).then(response => {
                commit('CUSTOMERS_LOADED');
                if (response.status === 200) {
                    commit('SET_CUSTOMER_SUCCESS');
                    commit('OPEN_MODAL')
                    dispatch('getCustomers')
                }
            }
        ).catch(err => {
            commit('SET_CUSTOMER_LOADED');
            commit('SET_CUSTOMER_ERROR');
            commit('OPEN_MODAL')
        })
    },
    async editProjectAddress({commit, dispatch}, payload) {
        commit('RESET_CUSTOMER_SUCCESS');
        commit('CUSTOMERS_LOADING');
        let newAddress = await dispatch('addAddress', payload.address)
        Api().put(`/api/Project/newaddress?projectId=${payload.id}&addresId=${newAddress.id}`).then(response => {
                commit('CUSTOMERS_LOADED');
                if (response.status === 200) {
                    commit('SET_CUSTOMER_SUCCESS');
                    commit('OPEN_MODAL')
                    dispatch('getCustomers')
                }
            }
        ).catch(err => {
            commit('CUSTOMERS_LOADED');
            commit('SET_CUSTOMER_ERROR');
            commit('OPEN_MODAL')
        })
    },
    deleteProject: ({commit, dispatch}, id) => {
        commit('RESET_CUSTOMER_SUCCESS');
        Api().delete(`/api/Project/${id}`).then(response => {
                commit('SET_CUSTOMER_SUCCESS');
                commit('OPEN_MODAL')
                dispatch('getCustomers')
            }
        ).catch(err => {
            commit('SET_CUSTOMER_ERROR');
            commit('OPEN_MODAL')
        })
    },
    editCustomer: ({commit, dispatch}, payload) => {
        commit('RESET_CUSTOMER_SUCCESS');
        Api().put('/api/Customer', payload).then(response => {
                commit('SET_CUSTOMER_SUCCESS');
                commit('OPEN_MODAL')
                dispatch('getCustomers')
            }
        ).catch(err => {
            commit('SET_CUSTOMER_ERROR');
            commit('OPEN_MODAL')
        })
    },
    async editCustomerAddress({commit, dispatch}, payload) {
        commit('RESET_CUSTOMER_SUCCESS');
        let newAddress = await dispatch('addAddress', payload.address)
        Api().put(`/api/Customer/newaddress?customerId=${payload.id}&addresId=${newAddress.id}`).then(response => {
                commit('SET_CUSTOMER_SUCCESS');
                commit('OPEN_MODAL')
                dispatch('getCustomers')
            }
        ).catch(err => {
            commit('SET_CUSTOMER_ERROR');
            commit('OPEN_MODAL')
        })
    },
    deleteCustomer: ({commit, dispatch}, id) => {
        commit('RESET_CUSTOMER_SUCCESS');
        Api().delete(`/api/Customer/${id}`).then(response => {
                commit('SET_CUSTOMER_SUCCESS');
                commit('OPEN_MODAL')
                dispatch('getCustomers')
            }
        ).catch(err => {
            commit('SET_CUSTOMER_ERROR');
            commit('OPEN_MODAL')
        })
    },
    suspendCustomer: ({commit, dispatch}, id) => {
        commit('RESET_CUSTOMER_SUCCESS');
        Api().put(`/api/Customer/suspendCustomer/${id}`).then(response => {
                commit('SET_CUSTOMER_SUCCESS');
                commit('OPEN_MODAL')
                dispatch('getCustomers')
            }
        ).catch(err => {
            commit('SET_CUSTOMER_ERROR');
            commit('OPEN_MODAL')
        })
    },
    suspendProject: ({commit, dispatch}, id) => {
        commit('RESET_CUSTOMER_SUCCESS');
        Api().put(`/api/Project/suspendProject/${id}`).then(response => {
                commit('SET_CUSTOMER_SUCCESS');
                commit('OPEN_MODAL')
                dispatch('getCustomers')
            }
        ).catch(err => {
            commit('SET_CUSTOMER_ERROR');
            commit('OPEN_MODAL')
        })
    },

}

const getters = {
    customers: state => {
        return state.customers;
    },
    customersLoading: state => {
        return state.customersLoading;
    },
    customerSuccess: state => {
        return state.customerSuccess;
    },
    showCustomerEditConfirmModal: state => {
        return state.showModal;
    },
    customerProjects: (state) => (id) => {
        return state.customers.find(customer => customer.id === id)
    },
    projectById: (state) => (customerId, id) => {
        return state.customers.find(customer => customer.id === customerId).projects.find(project => project.id === id)
    },
    projectName: (state) => (customerId, id) => {
        return state.customers.find(customer => customer.id === customerId).projects.find(project => project.id === id).name
    },
    projectColor: (state) => (customerId, id) => {
        return state.customers.find(customer => customer.id === customerId).projects.find(project => project.id === id).uiColorCode
    },
    projectAddress: (state) => (customerId, id) => {
        return state.customers.find(customer => customer.id === customerId).projects.find(project => project.id === id).address.addressLine1
    },
    projectsForSchedule: state => {
        let arr1 = state.customers.map(el => el.id);
        let arr2 = arr1.map(id => state.customers.find(customer => customer.id === id).projects)
        return arr2.flat();
    },
}


export default {
    state, mutations, actions, getters
}