import Api from "@/services/api";

const state = {
    loggedEmployee: {},
    loggedEmployeeSchedule: [],
    employeeLoading: false,
    loggedEmployeeError: false,
    loggedEmployeeSuccess: false,
    showEmployeeModal: false,
    showMarkAsDoneModal: false,
    workingProject: {},
    employeeHours: [],
    hoursLoading: false,
    projects: [],
};

const mutations = {
    'OPEN_DONE_MODAL'(state) {
        state.showMarkAsDoneModal = true;
    },
    'CLOSE_DONE_MODAL'(state) {
        state.showMarkAsDoneModal = false;
    },
    'SET_EMPLOYEE_LOADING'(state) {
        state.employeeLoading = true;
    },
    'SET_EMPLOYEE_LOADED'(state) {
        state.employeeLoading = false;
    },
    'SET_HOURS_LOADING'(state) {
        state.hoursLoading = true;
    },
    'SET_HOURS_LOADED'(state) {
        state.hoursLoading = false;
    },
    'OPEN_MODAL'(state) {
        state.showEmployeeModal = true;
    },
    'CLOSE_MODAL'(state) {
        state.showEmployeeModal = false;
    },
    'SET_EMPLOYEE_ERROR'(state) {
        state.loggedEmployeeError = true;
    },
    'SET_EMPLOYEE_ERROR_RESOLVE'(state) {
        state.loggedEmployeeError = false;
    },
    'SET_EMPLOYEE_SUCCESS'(state) {
        state.loggedEmployeeSuccess = true;
    },
    'RESET_EMPLOYEE_SUCCESS'(state) {
        state.loggedEmployeeSuccess = false;
    },
    'SET_EMPLOYEE_HOURS'(state, data) {
        let smth = data.hoursData.map(el => parseInt(el.duration))
        let sum = smth.reduce((a, b) => a + b, 0)
        let obj = {value: sum, employeeId: data.employeeId}
        state.employeeHours = [...state.employeeHours, obj]
    },
    'SET_EMPLOYEE'(state, employee) {
        state.loggedEmployee = {
            id: employee.id,
            firstName: employee.firstName,
            lastName: employee.lastName,
            uiColorCode: employee.uiColorCode,
            phone: employee.phone,
            isActive: employee.isActive,
            address: {
                addressLine1: employee.address.addressLine1,
                addressLine2: employee.address.addressLine2,
                city: employee.address.city,
                zip: employee.address.zip,
                country: employee.address.country
            },
        };
    },
    'SET_NEW_PROJECT_STATUS'(state, id) {
        state.loggedEmployeeSchedule.find(el => el.id === id).isDone = true;
    },
    'ADD_PROJECT'(state, data) {
        state.projects = [...state.projects, data]
    },
    'SET_WORKING_PROJECT'(state, project) {
        state.workingProject = {
            id: project.id,
            customerId: project.customerId,
            name: project.name,
            isHourlyPaid: project.isHourlyPaid,
            hourlyRate: project.hourlyRate,
            uiColorCode: project.uiColorCode,
            pricePerPiece: project.pricePerPiece,
            projectDescription: project.projectDescription,
            taskDescription: project.taskDescription,
            isActive: project.isActive,
            address: {
                addressLine1: project.address.addressLine1,
                addressLine2: project.address.addressLine2,
                city: project.address.city,
                zip: project.address.zip,
                country: project.address.country
            }
        };
    },
    'SET_EMPLOYEE_SCHEDULE'(state, schedule) {
        state.loggedEmployeeSchedule = schedule.map(job => ({
            id: job.id,
            workingDate: job.workingDate,
            startTime: job.startTime,
            endTime: job.endTime,
            todayTasks: job.todayTasks,
            customerId: job.customerId,
            projectId: job.projectId,
            employeeIds: job.employeeIds,
            isDone: job.isDone,
        }));
    },
}

const actions = {
    closeEmployeeModal: ({commit}) => {
        commit('CLOSE_MODAL');
    },
    openMarkAsDoneModal: ({commit}) => {
        commit('OPEN_DONE_MODAL');
    },
    closeMarkAsDoneModal: ({commit}) => {
        commit('CLOSE_DONE_MODAL');
    },
    async getWorkedHours({commit}, payload) {
        commit('SET_HOURS_LOADING');
        await Api().get(`/api/Employee/hours/${payload.id}?startDate=${payload.startDate}&endDate=${payload.endDate}`).then(response => {
                commit('SET_HOURS_LOADED');
                commit('SET_EMPLOYEE_HOURS', {employeeId: payload.id, hoursData: response.data})
            }
        ).catch(err => {
            commit('SET_EMPLOYEE_LOADED');
            commit('SET_EMPLOYEE_ERROR');
        })
    },
    async getLoggedEmployee({commit}, payload) {
        commit('SET_EMPLOYEE_LOADING');
        commit('RESET_EMPLOYEE_SUCCESS');
        await Api().get(`/api/Employee/user/${payload}`).then(response => {
                commit('SET_EMPLOYEE_LOADED');
                commit('SET_EMPLOYEE_SUCCESS');
                commit('SET_EMPLOYEE', response.data)
            }
        ).catch(err => {
            commit('SET_EMPLOYEE_LOADED');
            commit('SET_EMPLOYEE_ERROR');
        })
    },
    editEmployeeProfile: ({commit, dispatch}, payload) => {
        commit('SET_EMPLOYEE_LOADING');
        commit('RESET_EMPLOYEE_SUCCESS');
        Api().put(`/api/Employee/${payload.id}`, payload.employee).then(response => {
                commit('SET_EMPLOYEE_LOADED');
                commit('SET_EMPLOYEE_SUCCESS');
                commit('OPEN_MODAL')
                dispatch('getLoggedEmployee', payload.id)

            }
        ).catch(err => {
            commit('SET_EMPLOYEE_LOADED');
            commit('SET_EMPLOYEE_ERROR');
            commit('OPEN_MODAL')
        })
    },
    markAsDone: ({commit}, payload) => {
        commit('SET_EMPLOYEE_LOADING');
        commit('RESET_EMPLOYEE_SUCCESS');
        Api().put(`/api/Schedule/isDone/${payload}`).then(response => {
                commit('SET_EMPLOYEE_LOADED');
                commit('SET_EMPLOYEE_SUCCESS');
                commit('SET_NEW_PROJECT_STATUS', payload);
                commit('OPEN_MODAL')
            }
        ).catch(err => {
            commit('SET_EMPLOYEE_LOADED');
            commit('SET_EMPLOYEE_ERROR');
            commit('OPEN_MODAL')
        })
    },
    async editEmployeeAddress({commit, dispatch}, payload) {
        commit('SET_EMPLOYEE_LOADING');
        commit('RESET_EMPLOYEE_SUCCESS');
        let newAddress = await dispatch('addAddress', payload.address)
        Api().put(`/api/Employee/newaddress/${payload.id}?addresId=${newAddress.id}`).then(response => {
                commit('SET_EMPLOYEE_LOADED');
                commit('SET_EMPLOYEE_SUCCESS');
                commit('OPEN_MODAL')
                dispatch('getLoggedEmployee', payload.id)
            }
        ).catch(err => {
            commit('SET_EMPLOYEE_LOADED');
            commit('SET_EMPLOYEE_ERROR');
            commit('OPEN_MODAL')
        })
    },
    async loadEmployeeSchedule({commit, dispatch}, payload) {
        commit('SET_EMPLOYEE_LOADING');
        await Api().get(`/api/Schedule/2020-01-01/2022-01-01/${payload}`).then(response => {
                commit('SET_EMPLOYEE_SCHEDULE', response.data);
                response.data.map(job => {
                    dispatch('addProjectById', job.projectId)
                })
                commit('SET_EMPLOYEE_LOADED');
            }
        ).catch(err => {
            commit('SET_EMPLOYEE_LOADED');
            commit('SET_EMPLOYEE_ERROR');
            commit('OPEN_MODAL')
        })
    },
    async addProjectById({commit}, payload) {
        await Api().get(`/api/Project?projectId=${payload}`).then(response => {
                commit('ADD_PROJECT', response.data);
            }
        ).catch(err => {
        })
    },
    async getProjectById({commit}, payload) {
        await Api().get(`/api/Project?projectId=${payload}`).then(response => {
                commit('SET_WORKING_PROJECT', response.data);
            }
        ).catch(err => {

        })
    }

}

const getters = {
    loggedEmployee: state => {
        return state.loggedEmployee;
    },
    workingProject: state => {
        return state.workingProject;
    },
    loggedEmployeeSchedule: state => {
        return state.loggedEmployeeSchedule;
    },
    loggedEmployeeScheduleByDate: (state) => (date) => {
        return state.loggedEmployeeSchedule.filter(job => date.getDate() === new Date(job.workingDate).getDate() && date.getMonth() === new Date(job.workingDate).getMonth());
    },
    employeeLoading: state => {
        return state.employeeLoading;
    },
    loggedEmployeeError: state => {
        return state.loggedEmployeeError;
    },
    loggedEmployeeSuccess: state => {
        return state.loggedEmployeeSuccess;
    },
    showEmployeeModal: state => {
        return state.showEmployeeModal;
    },
    projects: state => {
        return state.projects;
    },
    showMarkAsDoneModal: state => {
        return state.showMarkAsDoneModal;
    },
    workedHoursById: (state) => (id) => {
        let smth = state.employeeHours.find(employee => employee.employeeId === id)
        if (smth === undefined || smth === null) {
            return 0
        } else {
            return smth.value
        }

    },
    scheduleProjectById: (state) => (id) => {
        return state.projects.find(project => project.id === id)
    },

}


export default {
    state, mutations, actions, getters
}