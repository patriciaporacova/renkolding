import Api from "@/services/api";

const state = {
    employees: [],
    employeesLoading: false,
    employeeError: false,
    employeeSuccess: false,
    showModal: false,
};

const mutations = {
    'SET_EMPL_LOADING'(state) {
        state.employeesLoading = true;
    },
    'SET_EMPL_LOADED'(state) {
        state.employeesLoading = false;
    },
    'OPEN_MODAL'(state) {
        state.showModal = true;
    },
    'CLOSE_MODAL'(state) {
        state.showModal = false;
    },
    'SET_EMPL_ERROR'(state) {
        state.employeeError = true;
    },
    'SET_EMPL_ERROR_RESOLVE'(state) {
        state.employeeError = false;
    },
    'SET_EMPL_SUCCESS'(state) {
        state.employeeSuccess = true;
    },
    'RESET_EMPL_SUCCESS'(state) {
        state.employeeSuccess = false;
    },
    'SET_EMPLOYEES'(state, employees) {
        state.employees = employees.map(employee => ({
            id: employee.id,
            firstName: employee.firstName,
            lastName: employee.lastName,
            uiColorCode: employee.uiColorCode,
            phone: employee.phone,
            isActive: employee.isActive,
            address: {
                addressLine1: employee.address.addressLine1,
                addressLine2: employee.address.addressLine2,
                city: employee.address.city,
                zip: employee.address.zip,
                country: employee.address.country
            },
        }));
    },
}

const actions = {
    closeShowingModal: ({commit}) => {
        commit('CLOSE_MODAL');
    },
    async getEmployees({commit, dispatch}) {
        commit('SET_EMPL_LOADING');
        commit('RESET_EMPL_SUCCESS');
        await Api().get('/api/Employee').then(response => {
                commit('SET_EMPL_LOADED');
                commit('SET_EMPL_SUCCESS');
                //commit('SET_EMPLOYEES', response.data)
                commit('SET_EMPLOYEES', response.data)
                response.data.map(el => {
                    dispatch('getWorkedHours', {id: 10, startDate: '2021-06-01', endDate: '2021-06-30'})
                })
            }
        ).catch(err => {
            commit('SET_EMPL_LOADED');
            commit('SET_EMPL_ERROR');
        })
    },
    addEmployee: ({commit, dispatch}, payload) => {
        commit('RESET_EMPL_SUCCESS');
        commit('SET_EMPL_LOADING');
        Api().post(`/api/Employee?username=${payload.username}&password=${payload.password}&isAdmin=${payload.isAdmin}
`, payload.employee).then(response => {
                commit('SET_EMPL_LOADED');
                commit('SET_EMPL_SUCCESS');
                commit('OPEN_MODAL')
                dispatch('getEmployees')
            }
        ).catch((error) => {
            if (error.response.status === 409) {
                alert(error.response.data);
            }
        });
    },
    editEmployee: ({commit, dispatch}, payload) => {
        commit('RESET_EMPL_SUCCESS');
        commit('SET_EMPL_LOADING');
        Api().put(`/api/Employee/${payload.id}`, payload.employee).then(response => {
                commit('SET_EMPL_LOADED');
                if (response.status === 200) {
                    commit('SET_EMPL_SUCCESS');
                    commit('OPEN_MODAL')
                    dispatch('getEmployees')
                }
            }
        ).catch(err => {
            commit('SET_EMPL_LOADED');
            commit('SET_EMPL_ERROR');
            commit('OPEN_MODAL')
        })
    },
    async editEmployeeAddress({commit, dispatch}, payload) {
        commit('RESET_EMPL_SUCCESS');
        commit('SET_EMPL_LOADING');
        let newAddress = await dispatch('addAddress', payload.address)
        Api().put(`/api/Employee/newaddress/${payload.id}?addresId=${newAddress.id}`).then(response => {
                commit('SET_EMPL_LOADED');
                if (response.status === 200) {
                    commit('SET_EMPL_SUCCESS');
                    commit('OPEN_MODAL')
                    dispatch('getEmployees')
                }
            }
        ).catch(err => {
            commit('SET_EMPL_LOADED');
            commit('SET_EMPL_ERROR');
            commit('OPEN_MODAL')
        })
    },
    deleteEmployee: ({commit, dispatch}, id) => {
        commit('RESET_EMPL_SUCCESS');
        commit('SET_EMPL_LOADING');
        Api().delete(`/api/Employee/${id}`).then(response => {
                commit('SET_EMPL_LOADED');
                commit('SET_EMPL_SUCCESS');
                if (response.status === 200) {
                    dispatch('getEmployees')
                }
            }
        ).catch(err => {
            commit('SET_EMPL_LOADED');
            commit('SET_EMPL_ERROR');
        })
    },
    suspendEmployee: ({commit, dispatch}, id) => {
        commit('RESET_EMPL_SUCCESS');
        commit('SET_EMPL_LOADING');
        Api().put(`/api/Employee/suspend/${id}`).then(response => {
                commit('SET_EMPL_LOADED');
                commit('SET_EMPL_SUCCESS');
                dispatch('getEmployees')
            }
        ).catch(err => {
            commit('SET_EMPL_LOADED');
            commit('SET_EMPL_ERROR');
        })
    },

}

const getters = {
    employees: state => {
        return state.employees;
    },
    employeesLoading: state => {
        return state.employeesLoading;
    },
    employeeError: state => {
        return state.employeeError;
    },
    employeeSuccess: state => {
        return state.employeeSuccess;
    },
    employeeById: (state) => (id) => {
        return state.employees.find(employee => employee.id === id)
    },
    showEditConfirmModal: state => {
        return state.showModal;
    },

}


export default {
    state, mutations, actions, getters
}