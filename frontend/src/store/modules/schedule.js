import Api from "@/services/api";

const state = {
    schedule: [],
    isSchedule: false,
    scheduleForCurrentMonth: [],
    scheduleSuccess: false,
    isEditModalOpened: false,
    showScheduleModal: false,
};

const mutations = {
    'OPEN_MODAL'(state) {
        state.showScheduleModal = true;
    },
    'CLOSE_MODAL'(state) {
        state.showScheduleModal = false;
    },
    'SET_SCHEDULE_SUCCESS'(state) {
        state.scheduleSuccess = true;
    },
    'RESET_SCHEDULE_SUCCESS'(state) {
        state.scheduleSuccess = false;
    },
    'SET_SCHEDULE'(state, schedule) {
        state.schedule = schedule.map(job => ({
            id: job.id,
            workingDate: job.workingDate,
            startTime: job.startTime,
            endTime: job.endTime,
            todayTasks: job.todayTasks,
            customerId: job.customerId,
            projectId: job.projectId,
            employeeIds: job.employeeIds,
            isDone: job.isDone,
        }));
    },
    'OPEN_SCHEDULE'(state) {
        state.isSchedule = true;
    },
    'CLOSE_SCHEDULE'(state) {
        state.isSchedule = false;
    },
    'OPEN_EDIT_MODAL'(state) {
        state.isEditModalOpened = true;
    },
    'CLOSE_EDIT_MODAL'(state) {
        state.isEditModalOpened = false;
    },
    'SET_THIS_MONTHS_SCHEDULE'(state, schedule) {
        state.scheduleForCurrentMonth = schedule.map(job => ({
            id: job.id,
            workingDate: job.workingDate,
            startTime: job.startTime,
            endTime: job.endTime,
            todayTasks: job.todayTasks,
            customerId: job.customerId,
            projectId: job.projectId,
            employeeIds: job.employeeIds,
        }));
    },
}

const actions = {
    openSchedule: ({commit}) => {
        commit('OPEN_SCHEDULE');

    },
    closeSchedule: ({commit}) => {
        commit('CLOSE_SCHEDULE');

    },
    openEditModal: ({commit}) => {
        commit('OPEN_EDIT_MODAL');

    },
    closeEditModal: ({commit}) => {
        commit('CLOSE_EDIT_MODAL');
    },
    closeScheduleShowingModal: ({commit}) => {
        commit('CLOSE_MODAL');
    },
    getScheduleForCurrentMonth: ({commit}) => {
        Api().get('/api/Schedule/current-month').then(response => {
                commit('SET_THIS_MONTHS_SCHEDULE', response.data)
            }
        )
    },
    getSchedule: ({commit}) => {
        Api().get("/api/Schedule/2020-01-01/2022-01-01").then(response => {
                commit('SET_SCHEDULE', response.data)
            }
        )
    },
    getScheduleByEmployee: ({commit}, payload) => {
        Api().get(`/api/Schedule/${payload}`).then(response => {
                commit('SET_SCHEDULE', response.data)
            }
        )
    },
    addJob: ({commit, dispatch}, payload) => {
        commit('RESET_SCHEDULE_SUCCESS');
        Api().post('/api/Schedule', payload).then(response => {
                commit('SET_SCHEDULE_SUCCESS');
                commit('OPEN_MODAL')
                dispatch('getSchedule')
            }
        ).catch(err => {
            commit('OPEN_MODAL')
        })
    },
    editJob: ({commit, dispatch}, payload) => {
        commit('RESET_SCHEDULE_SUCCESS');
        Api().put('/api/Schedule', payload).then(response => {
                commit('SET_SCHEDULE_SUCCESS');
                commit('OPEN_MODAL')
                dispatch('getSchedule')
            }
        ).catch(err => {
            commit('OPEN_MODAL')
        })
    },
    assignEmployee: ({commit, dispatch}, payload) => {
        Api().post(`/api/Schedule/assignEmployee/${payload.employeeId}/${payload.jobId}`, payload).then(response => {
                let schedule = [...state.schedule, response.data]
                commit('SET_SCHEDULE', schedule)
            }
        )
    },
    addRepeatedJob: ({commit, dispatch}, payload) => {
        commit('RESET_SCHEDULE_SUCCESS');
        Api().post(`/api/Schedule/${payload.repeats}/${payload.endDate}`, payload.newJob).then(response => {
                commit('SET_SCHEDULE_SUCCESS');
                commit('OPEN_MODAL')
                dispatch('getSchedule')
            }
        ).catch(err => {
            commit('OPEN_MODAL')
        })
    },
}

const getters = {
    schedule: state => {
        return state.schedule;
    },
    editModal: state => {
        return state.isEditModalOpened;
    },
    scheduleSuccess: state => {
        return state.scheduleSuccess;
    },
    showScheduleEditConfirmModal: state => {
        return state.showScheduleModal;
    },
    isScheduleOpened: state => {
        return state.isSchedule;
    },
    monthlySchedule: state => {
        return state.schedule;
    },
    scheduleByEmployee: (state) => (id, date) => {
        return state.schedule.filter(job => job.employeeIds.includes(id) && date.getDate() === new Date(job.workingDate).getDate() && date.getMonth() === new Date(job.workingDate).getMonth())
    },
    scheduleByDate: (state) => (date) => {
        return state.schedule.filter(job => date.getDate() === new Date(job.workingDate).getDate() && date.getMonth() === new Date(job.workingDate).getMonth());
    },
}


export default {
    state, mutations, actions, getters
}