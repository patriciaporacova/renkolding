import Vue from 'vue';
import Vuex from 'vuex'
import customer from "./modules/customer.js";
import employees from "./modules/employees.js";
import employee from "./modules/employee.js";
import auth from "./modules/auth.js";
import schedule from "./modules/schedule.js";
import address from "./modules/address.js";

Vue.use(Vuex);

export default new Vuex.Store({modules: {customer, auth, employees, schedule, address, employee}});